using Newtonsoft.Json.Linq;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;

var builder = WebApplication.CreateBuilder(args);

var ocelotJson = new JObject();
var folder = builder.Environment.IsDevelopment() ? "ConfigurationDevelop" : "Configuration";
foreach (var jsonFilename in Directory.EnumerateFiles(folder, "ocelot.*.json", SearchOption.AllDirectories))
{
    using (StreamReader fi = File.OpenText(jsonFilename))
    {
        var json = JObject.Parse(fi.ReadToEnd());
        ocelotJson.Merge(json, new JsonMergeSettings
        {
            MergeArrayHandling = MergeArrayHandling.Union
        });
    }
}

File.WriteAllText("ocelot.json", ocelotJson.ToString());

IConfiguration configuration = new ConfigurationBuilder()
                                .SetBasePath(builder.Environment.ContentRootPath)
                                .AddJsonFile("ocelot.json", optional: false, reloadOnChange: true)
                                .AddJsonFile($"ocelot.{builder.Environment.EnvironmentName}.json",
                                    optional: true, reloadOnChange: true)
                                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                                .AddJsonFile($"appsettings.{builder.Environment.EnvironmentName}.json",
                                    optional: true, reloadOnChange: true)
                                .AddEnvironmentVariables()
                                .Build();

builder.Services.AddOcelot(configuration);

var app = builder.Build();

app.UseStaticFiles();
app.UseOcelot()
    .Wait();

app.Run();
