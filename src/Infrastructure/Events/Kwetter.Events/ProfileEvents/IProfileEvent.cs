﻿namespace Kwetter.Events.ProfileEvents;

public interface IProfileEvent
{
    Guid Id { get; }
}
