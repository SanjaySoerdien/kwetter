﻿namespace Kwetter.Events.ProfileEvents;

public record ProfileBioChangedEvent(Guid Id, string Bio) : IProfileEvent;