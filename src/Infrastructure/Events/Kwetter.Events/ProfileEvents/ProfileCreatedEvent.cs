﻿namespace Kwetter.Events.ProfileEvents;

public record ProfileCreatedEvent(Guid Id, string Usertag, string Username) : IProfileEvent
{
    public string? Bio { get; set; }
    public string? Location { get; set; }
    public string? Website { get; set; }
    public DateTime DateCreated { get; set; } = DateTime.Now;
}
