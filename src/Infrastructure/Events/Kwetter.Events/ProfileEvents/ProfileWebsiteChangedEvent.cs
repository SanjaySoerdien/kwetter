﻿namespace Kwetter.Events.ProfileEvents;

public record ProfileWebsiteChangedEvent(Guid Id, string Website) : IProfileEvent;
