﻿namespace Kwetter.Events.ProfileEvents;

public record ProfileUsertagChangedEvent(Guid Id, string Usertag) : IProfileEvent;
