﻿namespace Kwetter.Events.ProfileEvents;

public record ProfileDeletedEvent(Guid Id) : IProfileEvent;