﻿namespace Kwetter.Events.ProfileEvents;

public record ProfileLocationChangedEvent(Guid Id, string Location) : IProfileEvent;
