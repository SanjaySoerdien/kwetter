﻿namespace Kwetter.Events.ProfileEvents;

public record ProfileUsernameChangedEvent(Guid Id, string Username) : IProfileEvent;
