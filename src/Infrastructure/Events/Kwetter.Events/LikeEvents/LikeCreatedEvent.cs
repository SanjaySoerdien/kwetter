﻿using System.ComponentModel.DataAnnotations;

namespace Kwetter.Events.LikeEvents;

public record LikeCreatedEvent([Required] int TweetId = default);