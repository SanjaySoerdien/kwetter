﻿using System.ComponentModel.DataAnnotations;

namespace Kwetter.Events.LikeEvents;

public record LikeDeletedEvent([Required] int TweetId = default);