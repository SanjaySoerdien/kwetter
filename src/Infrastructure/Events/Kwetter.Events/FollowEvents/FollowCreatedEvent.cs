﻿using System.ComponentModel.DataAnnotations;

namespace Kwetter.Events.FollowEvents;

public record FollowCreatedEvent([Required] Guid FollowerId, [Required] Guid FollowedId);
