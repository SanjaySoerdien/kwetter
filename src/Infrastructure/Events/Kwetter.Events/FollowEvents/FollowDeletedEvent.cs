﻿using System.ComponentModel.DataAnnotations;

namespace Kwetter.Events.FollowEvents;

public record FollowDeletedEvent([Required] Guid FollowerId, [Required] Guid FollowedId);