﻿using System.ComponentModel.DataAnnotations;

namespace Kwetter.Events.TweetEvents;

public record TweetDeletedEvent([Required] int Id);