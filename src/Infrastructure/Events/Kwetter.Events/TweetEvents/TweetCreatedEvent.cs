﻿using System.ComponentModel.DataAnnotations;

namespace Kwetter.Events.TweetEvents;

public record TweetCreatedEvent([Required] int Id = default);
