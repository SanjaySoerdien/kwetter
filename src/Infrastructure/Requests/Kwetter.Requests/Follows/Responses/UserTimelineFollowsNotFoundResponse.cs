﻿namespace Kwetter.Requests.Follows.Responses;

public record UserTimelineFollowsNotFoundResponse;
