﻿namespace Kwetter.Requests.Follows.Responses;

public record UserTimelineFollowsResponse
{
    public IEnumerable<Guid> FollowIds { get; set; } = new List<Guid>();
}