﻿namespace Kwetter.Requests.Follows.Requests;

public record UserTimelineFollowsRequest(Guid UserId);