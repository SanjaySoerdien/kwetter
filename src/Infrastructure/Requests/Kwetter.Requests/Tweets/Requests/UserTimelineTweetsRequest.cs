﻿namespace Kwetter.Requests.Tweets.Requests;

public record UserTimelineTweetsRequest(IEnumerable<Guid> FollowerIds);
