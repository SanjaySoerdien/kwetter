﻿namespace Kwetter.Requests.Tweets.Requests;

public record HomeTimelineRequest(Guid UserId);