﻿namespace Kwetter.Requests.Tweets.Responses;
public record UserTimelineTweetsNotFoundResponse;