﻿namespace Kwetter.Requests.Tweets.Responses;

public record HomeTimelineNotFoundResponse;
