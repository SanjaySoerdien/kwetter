﻿using Kwetter.Requests.Tweets.Responses.Model;

namespace Kwetter.Requests.Tweets.Responses;

public class UserTimelineTweetsResponse
{
    public IEnumerable<TweetEventModel> Tweets { get; set; } = new List<TweetEventModel>();
}
