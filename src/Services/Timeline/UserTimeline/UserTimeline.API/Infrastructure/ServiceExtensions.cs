﻿namespace UserTimeline.API.Infrastructure
{
    public static class ServiceExtensions
    {
        private readonly static string origins = "user-timeline-origins";
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(name: origins,
                    policy =>
                    {
                        policy.WithOrigins("https://localhost:8080/", "http://localhost:8081/");
                    });
            });

            return services;
        }

        public static IServiceCollection AddSwagger(this IServiceCollection services)
        {
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();

            return services;
        }

        public static WebApplication InitApp(this WebApplication app)
        {
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseCors(origins);
            app.MapControllers();
            return app;
        }
    }
}
