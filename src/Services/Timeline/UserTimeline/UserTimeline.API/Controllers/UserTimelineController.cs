﻿using UserTimeline.Data.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace UserTimeline.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UserTimelineController : ControllerBase
    {
        private readonly IUserTimelineService _userTimelineService;

        public UserTimelineController(IUserTimelineService userTimelineService)
        {
            _userTimelineService = userTimelineService;
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync([FromRoute] Guid Id)
        {
            var userTimeline = await _userTimelineService.GetByIdAsync(Id);

            return Ok(userTimeline);
        }
    }
}
