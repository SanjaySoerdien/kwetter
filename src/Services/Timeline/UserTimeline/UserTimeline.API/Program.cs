using UserTimeline.API.Infrastructure;
using UserTimeline.Data.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddUserTimelineDataServices(builder.Configuration);

builder.Services.AddControllers();
builder.Services.AddServices()
    .AddSwagger();

var app = builder.Build()
                .InitApp();


app.Run();
