﻿using UserTimeline.Data.Services.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace UserTimeline.Data.Services;
public static class StartupConfig
{
    public static IServiceCollection AddUserTimelineDataServices(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddServices()
            .AddUserTimelineData(configuration);

        return services;
    }
}
