﻿using UserTimeline.Data.Models.UserTimeline;

namespace UserTimeline.Data.Services.Interfaces;

public interface IUserTimelineService
{
    Task<IEnumerable<TweetDto>> GetByIdAsync(Guid id);
}
