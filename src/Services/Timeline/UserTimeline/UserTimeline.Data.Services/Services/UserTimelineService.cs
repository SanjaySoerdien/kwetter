﻿using UserTimeline.Data.Models.UserTimeline;
using UserTimeline.Data.Queries;
using UserTimeline.Data.Services.Interfaces;
using MediatR;

namespace UserTimeline.Data.Services.Services;

public class UserTimelineService : IUserTimelineService
{
    private readonly IMediator _mediator;

    public UserTimelineService(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task<IEnumerable<TweetDto>> GetByIdAsync(Guid id)
    {
        return await _mediator.Send(new GetUserTimelineQuery(id));
    }
}
