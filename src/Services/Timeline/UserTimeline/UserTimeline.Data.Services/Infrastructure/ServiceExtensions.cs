﻿using Microsoft.Extensions.DependencyInjection;
using UserTimeline.Data.Services.Interfaces;
using UserTimeline.Data.Services.Services;

namespace UserTimeline.Data.Services.Infrastructure
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services
                .AddScoped<IUserTimelineService, UserTimelineService>();
            return services;
        }
    }
}
