﻿using System.ComponentModel.DataAnnotations;

namespace UserTimeline.Data.Models.UserTimeline;

public record TweetDto
{
    [Required]
    public int Id { get; set; } = default!;

    public Guid ProfileId { get; set; }

    [Required]
    [MaxLength(64)]
    public string ProfileUsername { get; set; } = default!;

    [Required]
    [MaxLength(64)]
    [RegularExpression(@"^\S*$", ErrorMessage = "No white space allowed")]
    public string ProfileUsertag { get; set; } = default!;

    [Required]
    [MaxLength(240)]
    public string Content { get; init; } = default!;

    [Required]
    public int Likes { get; set; } = 0;
    [Required]
    public DateTime DateCreated { get; set; }
}
