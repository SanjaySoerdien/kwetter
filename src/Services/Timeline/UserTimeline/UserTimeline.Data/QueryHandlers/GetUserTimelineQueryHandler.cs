﻿using AutoMapper;
using Kwetter.Requests.Follows.Requests;
using Kwetter.Requests.Follows.Responses;
using Kwetter.Requests.Tweets.Requests;
using Kwetter.Requests.Tweets.Responses;
using MassTransit;
using MediatR;
using StackExchange.Redis;
using System.Text.Json;
using UserTimeline.Data.Configuration;
using UserTimeline.Data.Models.UserTimeline;
using UserTimeline.Data.Queries;

namespace UserTimeline.Data.QueryHandlers;

public class GetUserTimelineQueryHandler : IRequestHandler<GetUserTimelineQuery, IEnumerable<TweetDto>?>
{
    private readonly IRequestClient<UserTimelineFollowsRequest> _followClient;
    private readonly IRequestClient<UserTimelineTweetsRequest> _tweetClient;
    private readonly IMapper _mapper;
    private readonly IConnectionMultiplexer _connectionMultiplexer;

    public GetUserTimelineQueryHandler(IRequestClient<UserTimelineFollowsRequest> followClient, IRequestClient<UserTimelineTweetsRequest> tweetClient,
        IMapper mapper, IConnectionMultiplexer connectionMultiplexer)
    {
        _followClient = followClient;
        _tweetClient = tweetClient;
        _mapper = mapper;
        _connectionMultiplexer = connectionMultiplexer;
    }

    public async Task<IEnumerable<TweetDto>?> Handle(GetUserTimelineQuery request, CancellationToken cancellationToken)
    {
        //try to hit cache
        var db = _connectionMultiplexer.GetDatabase();
        var tryFromCache = await db.StringGetSetExpiryAsync(request.Id.ToString(), expiry: Constants.EXPIRY_SECONDS);
        if (tryFromCache != RedisValue.Null)
        {
            var tweets = JsonSerializer.Deserialize<IEnumerable<TweetDto>>(tryFromCache);
            return tweets;
        }

        var follows = await GetUserFollows(request.Id, cancellationToken);
        if (follows == null) return null;
        if (!follows.Any()) return new List<TweetDto>();
        return await GetUserTweets(follows, request.Id, db, cancellationToken);
    }

    private async Task<IEnumerable<Guid>?> GetUserFollows(Guid id, CancellationToken cancellationToken = default)
    {
        //send request over bus
        var followRequest = new UserTimelineFollowsRequest(id);
        var response = await _followClient.GetResponse<UserTimelineFollowsResponse, UserTimelineFollowsNotFoundResponse>(followRequest, cancellationToken);
        if (response.Is(out Response<UserTimelineFollowsResponse>? followsResponse))
        {
            return followsResponse!.Message.FollowIds;
        }
        else if (response.Is(out Response<UserTimelineFollowsNotFoundResponse> _))
        {
            return null;
        }
        return null;
    }

    private async Task<IEnumerable<TweetDto>?> GetUserTweets(IEnumerable<Guid> followIds, Guid userGuid, IDatabase db, CancellationToken cancellationToken)
    {
        var tweetsRequest = new UserTimelineTweetsRequest(followIds);
        var response = await _tweetClient.GetResponse<UserTimelineTweetsResponse, UserTimelineTweetsNotFoundResponse>(tweetsRequest, cancellationToken);
        if (response.Is(out Response<UserTimelineTweetsResponse>? followsResponse))
        {
            var tweets = _mapper.Map<IEnumerable<TweetDto>>(followsResponse!.Message.Tweets);
            var json = JsonSerializer.Serialize(tweets);
            await db.StringSetAsync(userGuid.ToString(), json, expiry: Constants.EXPIRY_SECONDS, flags: CommandFlags.FireAndForget);
            return tweets;
        }
        else if (response.Is(out Response<UserTimelineTweetsNotFoundResponse> _))
        {
            return null;
        }
        return null;
    }
}
