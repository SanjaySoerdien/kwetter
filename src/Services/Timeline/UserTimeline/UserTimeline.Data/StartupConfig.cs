﻿using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UserTimeline.Data.Infrastructure;

namespace UserTimeline.Data;
public static class StartupConfig
{
    public static IServiceCollection AddUserTimelineData(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddServices()
            .AddRedis(configuration)
            .AddRabbitMQMassTransit(configuration)
            .AddMediatR(typeof(StartupConfig).Assembly);

        return services;
    }
}
