﻿using UserTimeline.Data.Models.UserTimeline;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace UserTimeline.Data.Queries;

public record GetUserTimelineQuery([Required] Guid Id) : IRequest<IEnumerable<TweetDto>>;

