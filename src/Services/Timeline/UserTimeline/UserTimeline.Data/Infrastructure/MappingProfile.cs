﻿using AutoMapper;
using UserTimeline.Data.Models.UserTimeline;
using Kwetter.Requests.Tweets.Responses.Model;
using Tweet = UserTimeline.Data.Models.Tweet;

namespace UserTimeline.Data.Infrastructure;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<Tweet, TweetDto>().ReverseMap();
        CreateMap<TweetEventModel, TweetDto>().ReverseMap();
    }
}
