﻿namespace UserTimeline.Data.Configuration;

public static class Constants
{
    public readonly static TimeSpan EXPIRY_SECONDS = TimeSpan.FromSeconds(5);
    public readonly static TimeSpan EXPIRY_MINUTES = TimeSpan.FromMinutes(10);
}
