﻿using HomeTimeline.Data.Models.HomeTimeline;

namespace HomeTimeline.Data.Services.Interfaces;

public interface IHomeTimelineService
{
    Task<IEnumerable<TweetDto>> GetByIdAsync(Guid id);
}
