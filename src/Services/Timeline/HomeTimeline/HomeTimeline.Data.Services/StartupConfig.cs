﻿using HomeTimeline.Data.Services.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HomeTimeline.Data.Services;
public static class StartupConfig
{
    public static IServiceCollection AddHomeTimelineDataServices(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddServices()
            .AddHomeTimelineData(configuration);

        return services;
    }
}
