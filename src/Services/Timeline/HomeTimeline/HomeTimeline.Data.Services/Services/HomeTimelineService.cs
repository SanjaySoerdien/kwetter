﻿using HomeTimeline.Data.Models.HomeTimeline;
using HomeTimeline.Data.Queries;
using HomeTimeline.Data.Services.Interfaces;
using MediatR;

namespace HomeTimeline.Data.Services.Services;

public class HomeTimelineService : IHomeTimelineService
{
    private readonly IMediator _mediator;

    public HomeTimelineService(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task<IEnumerable<TweetDto>> GetByIdAsync(Guid id)
    {
        return await _mediator.Send(new GetHomeTimelineQuery(id));
    }
}
