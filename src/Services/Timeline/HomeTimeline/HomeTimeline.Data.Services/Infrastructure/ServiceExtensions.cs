﻿using HomeTimeline.Data.Services.Interfaces;
using HomeTimeline.Data.Services.Services;
using Microsoft.Extensions.DependencyInjection;

namespace HomeTimeline.Data.Services.Infrastructure
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services
                .AddScoped<IHomeTimelineService, HomeTimelineService>();
            return services;
        }
    }
}
