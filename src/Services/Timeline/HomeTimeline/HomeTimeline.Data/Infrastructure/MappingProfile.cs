﻿using AutoMapper;
using HomeTimeline.Data.Models.HomeTimeline;
using Kwetter.Requests.Tweets.Responses.Model;

namespace HomeTimeline.Data.Infrastructure;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<TweetEventModel, TweetDto>().ReverseMap();
    }
}
