﻿using HomeTimeline.Data.Infrastructure;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HomeTimeline.Data;
public static class StartupConfig
{
    public static IServiceCollection AddHomeTimelineData(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddServices()
            .AddRedis(configuration)
            .AddRabbitMQMassTransit(configuration)
            .AddMediatR(typeof(StartupConfig).Assembly);

        return services;
    }
}
