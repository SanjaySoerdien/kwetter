﻿using HomeTimeline.Data.Models.HomeTimeline;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace HomeTimeline.Data.Queries;

public record GetHomeTimelineQuery([Required] Guid Id) : IRequest<IEnumerable<TweetDto>>;

