﻿using AutoMapper;
using HomeTimeline.Data.Configuration;
using HomeTimeline.Data.Models.HomeTimeline;
using HomeTimeline.Data.Queries;
using Kwetter.Requests.Tweets.Requests;
using Kwetter.Requests.Tweets.Responses;
using MassTransit;
using MediatR;
using StackExchange.Redis;
using System.Text.Json;

namespace HomeTimeline.Data.QueryHandlers;

public class GetHomeTimelineQueryHandler : IRequestHandler<GetHomeTimelineQuery, IEnumerable<TweetDto>?>
{
    private readonly IRequestClient<HomeTimelineRequest> _client;
    private readonly IMapper _mapper;
    private readonly IConnectionMultiplexer _connectionMultiplexer;

    public GetHomeTimelineQueryHandler(IRequestClient<HomeTimelineRequest> client, IMapper mapper, IConnectionMultiplexer connectionMultiplexer)
    {
        _client = client;
        _mapper = mapper;
        _connectionMultiplexer = connectionMultiplexer;
    }

    public async Task<IEnumerable<TweetDto>?> Handle(GetHomeTimelineQuery request, CancellationToken cancellationToken)
    {
        //try to hit cache
        var db = _connectionMultiplexer.GetDatabase();
        var tryFromCache = await db.StringGetSetExpiryAsync(request.Id.ToString(), expiry: Constants.EXPIRY_SECONDS);
        if (tryFromCache != RedisValue.Null)
        {
            var tweets = JsonSerializer.Deserialize<IEnumerable<TweetDto>>(tryFromCache);
            return tweets;
        }

        //send request over bus
        var htRequest = new HomeTimelineRequest(request.Id);
        var response = await _client.GetResponse<HomeTimelineResponse, HomeTimelineNotFoundResponse>(htRequest, cancellationToken);

        if (response.Is(out Response<HomeTimelineResponse>? responseTimeline))
        {
            //if found, write to cache and return
            var tweets = _mapper.Map<IEnumerable<TweetDto>>(responseTimeline!.Message.Tweets);
            var json = JsonSerializer.Serialize(tweets);
            await db.StringSetAsync(request.Id.ToString(), json, expiry: Constants.EXPIRY_SECONDS, flags: CommandFlags.FireAndForget);
            return tweets;
        }
        else if (response.Is(out Response<HomeTimelineNotFoundResponse> _))
        {
            //if not found return empty object
            return new List<TweetDto>();
        }

        return null;
    }
}
