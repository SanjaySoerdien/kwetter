using HomeTimeline.API.Infrastructure;
using HomeTimeline.Data.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddHomeTimelineDataServices(builder.Configuration);

builder.Services.AddControllers();
builder.Services.AddServices()
    .AddSwagger();

var app = builder.Build()
                .InitApp();


app.Run();
