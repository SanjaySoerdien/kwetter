﻿using HomeTimeline.Data.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HomeTimeline.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class HomeTimelineController : ControllerBase
    {
        private readonly IHomeTimelineService _homeTimelineService;

        public HomeTimelineController(IHomeTimelineService homeTimelineService)
        {
            _homeTimelineService = homeTimelineService;
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync([FromRoute] Guid Id)
        {
            var homeTimeline = await _homeTimelineService.GetByIdAsync(Id);

            return Ok(homeTimeline);
        }
    }
}
