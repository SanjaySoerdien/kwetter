using Tweet.API.Infrastructure;
using Tweet.Data.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddTweetDataServices(builder.Configuration);

builder.Services.AddControllers();
builder.Services.AddServices(builder.Configuration)
    .AddSwagger();

var app = builder.Build()
                .InitApp();


app.Run();
