﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Tweet.Data.Services.Interfaces;
using Tweet.Data.Services.Models.Tweet;

namespace Tweet.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class TweetsController : ControllerBase
    {
        private readonly ITweetService _tweetService;

        public TweetsController(ITweetService tweetService)
        {
            _tweetService = tweetService;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var tweetDtos = await _tweetService.GetAsync();

            return Ok(tweetDtos);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetByIdAsync([FromRoute] int id)
        {
            var tweetDto = await _tweetService.GetByIdAsync(id);

            return Ok(tweetDto);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] TweetCreateDto model)
        {
            var tweetDto = await _tweetService.CreateAsync(model);
            if (tweetDto == null) return BadRequest("Unable to create tweet");
            return Created($"api/tweets/{tweetDto.Id}", tweetDto);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateByIdAsync([FromRoute] int id, [FromBody] TweetUpdateDto model)
        {
            var tweetDto = await _tweetService.UpdateByIdAsync(id, model);

            if (tweetDto is null) return BadRequest("Unable to update tweet");
            return Ok(tweetDto);
        }
        [AllowAnonymous]

        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteByIdAsync([FromRoute] int id)
        {
            var IsDeleted = await _tweetService.DeleteByIdAsync(id);

            if (IsDeleted)
            {
                return NoContent();
            }

            return BadRequest("Unable to delete tweet");
        }
    }
}
