﻿namespace Tweet.Data.Models
{
    public class Tweet
    {
        public int Id { get; set; } = default!;
        public string Content { get; set; } = default!;
        public int Likes { get; set; } = 0;
        public DateTime DateCreated { get; set; } = DateTime.Now;

        public Profile Profile { get; set; } = default!;
    }
}
