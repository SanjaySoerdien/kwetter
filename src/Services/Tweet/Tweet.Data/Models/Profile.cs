﻿namespace Tweet.Data.Models;

public class Profile
{
    public int Id { get; set; } = default!;
    public Guid ProfileId { get; set; }
    public string Usertag { get; set; } = default!;
    public string Username { get; set; } = default!;

    public virtual ICollection<Tweet> Tweets { get; set; } = new List<Tweet>();
}
