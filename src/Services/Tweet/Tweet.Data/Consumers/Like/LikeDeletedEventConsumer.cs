﻿using Kwetter.Events.LikeEvents;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Tweet.Data.Database;

namespace Tweet.Data.Consumers.Like;

public class LikeDeletedEventConsumer : IConsumer<LikeDeletedEvent>
{
    private readonly TweetDbContext _context;

    public LikeDeletedEventConsumer(TweetDbContext context)
    {
        _context = context;
    }

    public async Task Consume(ConsumeContext<LikeDeletedEvent> context)
    {
        var dbTweet = await _context.Tweets.SingleOrDefaultAsync(t => t.Id == context.Message.TweetId);
        if (dbTweet == null) return;
        dbTweet.Likes += -1;
        await _context.SaveChangesAsync();
    }
}