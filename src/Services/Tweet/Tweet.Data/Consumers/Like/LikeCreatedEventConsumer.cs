﻿using Kwetter.Events.LikeEvents;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Tweet.Data.Database;

namespace Tweet.Data.Consumers.Like;

public class LikeCreatedEventConsumer : IConsumer<LikeCreatedEvent>
{
    private readonly TweetDbContext _context;

    public LikeCreatedEventConsumer(TweetDbContext context)
    {
        _context = context;
    }

    public async Task Consume(ConsumeContext<LikeCreatedEvent> context)
    {
        var dbTweet = await _context.Tweets.SingleOrDefaultAsync(t => t.Id == context.Message.TweetId);
        if (dbTweet == null) return;
        dbTweet.Likes += 1;
        await _context.SaveChangesAsync();
    }
}

