﻿using Kwetter.Events.ProfileEvents;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Tweet.Data.Database;

namespace Tweet.Data.Consumers;

public class ProfileUsernameChangedEventConsumer : IConsumer<ProfileUsernameChangedEvent>
{
    private readonly TweetDbContext _context;

    public ProfileUsernameChangedEventConsumer(TweetDbContext context)
    {
        _context = context;
    }

    public async Task Consume(ConsumeContext<ProfileUsernameChangedEvent> context)
    {
        var dbProfile = await _context.Profiles.FirstOrDefaultAsync(x => x.ProfileId == context.Message.Id);
        if (dbProfile == null) return;
        dbProfile.Username = context.Message.Username;
        await _context.SaveChangesAsync();
    }
}
