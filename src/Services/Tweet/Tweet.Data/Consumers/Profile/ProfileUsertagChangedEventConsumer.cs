﻿using Kwetter.Events.ProfileEvents;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Tweet.Data.Database;

namespace Tweet.Data.Consumers;

public class ProfileUsertagChangedEventConsumer : IConsumer<ProfileUsertagChangedEvent>
{
    private readonly TweetDbContext _context;

    public ProfileUsertagChangedEventConsumer(TweetDbContext context)
    {
        _context = context;
    }

    public async Task Consume(ConsumeContext<ProfileUsertagChangedEvent> context)
    {
        var dbProfile = await _context.Profiles.FirstOrDefaultAsync(x => x.ProfileId == context.Message.Id);
        if (dbProfile == null) return;
        dbProfile.Usertag = context.Message.Usertag;
        await _context.SaveChangesAsync();
    }
}
