﻿using Kwetter.Events.ProfileEvents;
using Kwetter.Events.TweetEvents;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Tweet.Data.Database;

namespace Tweet.Data.Consumers;

public class ProfileDeletedEventConsumer : IConsumer<ProfileDeletedEvent>
{
    private readonly TweetDbContext _context;
    private readonly IPublishEndpoint _publishEndpoint;

    public ProfileDeletedEventConsumer(TweetDbContext context, IPublishEndpoint publishEndpoint)
    {
        _context = context;
        _publishEndpoint = publishEndpoint;
    }

    public async Task Consume(ConsumeContext<ProfileDeletedEvent> context)
    {
        var dbProfile = await _context.Profiles.FirstOrDefaultAsync(t => t.ProfileId == context.Message.Id);
        if (dbProfile == null) return;
        _context.Remove(dbProfile);
        foreach (var tweet in dbProfile.Tweets)
        {
            await _publishEndpoint.Publish(new TweetDeletedEvent(tweet.Id));
        }
        await _context.SaveChangesAsync();
    }
}
