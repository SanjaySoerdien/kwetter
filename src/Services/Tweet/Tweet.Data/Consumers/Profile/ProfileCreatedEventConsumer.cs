﻿using Kwetter.Events.ProfileEvents;
using MassTransit;
using Tweet.Data.Database;
using Tweet.Data.Models;

namespace Tweet.Data.Consumers;

public class ProfileCreatedEventConsumer : IConsumer<ProfileCreatedEvent>
{
    private readonly TweetDbContext _context;

    public ProfileCreatedEventConsumer(TweetDbContext context)
    {
        _context = context;
    }

    public async Task Consume(ConsumeContext<ProfileCreatedEvent> context)
    {
        var dbProfile = new Profile { ProfileId = context.Message.Id, Username = context.Message.Username, Usertag = context.Message.Username };
        await _context.AddAsync(dbProfile);
        await _context.SaveChangesAsync();
    }
}
