﻿using Kwetter.Requests.Tweets.Requests;
using Kwetter.Requests.Tweets.Responses;
using Kwetter.Requests.Tweets.Responses.Model;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Tweet.Data.Database;
namespace Tweet.Data.Consumers.Requests;

public class HomeTimelineRequestConsumer : IConsumer<HomeTimelineRequest>
{
    private readonly TweetDbContext _dbContext;

    public HomeTimelineRequestConsumer(TweetDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task Consume(ConsumeContext<HomeTimelineRequest> context)
    {
        var result = await _dbContext.Profiles
           .Include(x => x.Tweets
           .OrderByDescending(x => x.DateCreated))
           .FirstOrDefaultAsync(p => p.ProfileId == context.Message.UserId);

        if (result == null)
        {
            await context.RespondAsync(new HomeTimelineNotFoundResponse());
            return;
        }

        var response = new HomeTimelineResponse();
        var tweets = new List<TweetEventModel>();

        foreach (var tweet in result.Tweets)
        {
            var toAdd = new TweetEventModel
            {
                Id = tweet.Id,
                Content = tweet.Content,
                DateCreated = tweet.DateCreated,
                Likes = tweet.Likes,
                ProfileId = tweet.Profile.ProfileId,
                ProfileUsername = tweet.Profile.Username,
                ProfileUsertag = tweet.Profile.Usertag
            };
            tweets.Add(toAdd);
        }
        response.Tweets = tweets;
        await context.RespondAsync(response);
    }
}
