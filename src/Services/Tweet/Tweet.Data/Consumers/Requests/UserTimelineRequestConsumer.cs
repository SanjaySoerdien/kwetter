﻿using Kwetter.Requests.Tweets.Requests;
using Kwetter.Requests.Tweets.Responses;
using Kwetter.Requests.Tweets.Responses.Model;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Tweet.Data.Database;

namespace Tweet.Data.Consumers.Requests;

public class UserTimelineRequestConsumer : IConsumer<UserTimelineTweetsRequest>
{
    private readonly TweetDbContext _dbContext;

    public UserTimelineRequestConsumer(TweetDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task Consume(ConsumeContext<UserTimelineTweetsRequest> context)
    {
        var result = await _dbContext.Tweets.Include(x => x.Profile)
            .Where(t => context.Message.FollowerIds
                                            .Any(f => f == t.Profile.ProfileId))
            .OrderByDescending(t => t.DateCreated)
            .Take(1000)
            .ToListAsync();

        if (result == null)
        {
            await context.RespondAsync(new UserTimelineTweetsNotFoundResponse());
            return;
        }

        var response = new UserTimelineTweetsResponse();
        var tweets = response.Tweets.ToList();

        foreach (var tweet in result)
        {
            var toAdd = new TweetEventModel
            {
                Id = tweet.Id,
                Content = tweet.Content,
                DateCreated = tweet.DateCreated,
                Likes = tweet.Likes,
                ProfileId = tweet.Profile.ProfileId,
                ProfileUsername = tweet.Profile.Username,
                ProfileUsertag = tweet.Profile.Usertag
            };
            tweets.Add(toAdd);
        }
        response.Tweets = tweets;
        await context.RespondAsync(response);
    }
}
