﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Tweet.Data.Configuration;

public class ProfileConfiguration : IEntityTypeConfiguration<Models.Profile>
{
    public void Configure(EntityTypeBuilder<Models.Profile> builder)
    {
        builder.HasKey(x => x.Id);
        builder.HasIndex(x => x.Id);

        builder.Property(x => x.Username)
            .HasMaxLength(24)
            .HasColumnName("Username")
            .HasColumnType("nvarchar(64)")
            .IsRequired();

        builder.Property(x => x.Usertag)
            .HasColumnName("Usertag")
            .HasColumnType("nvarchar(64)")
            .IsRequired();

    }
}
