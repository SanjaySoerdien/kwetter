﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Tweet.Data.Configuration;

public class TweetConfiguration : IEntityTypeConfiguration<Models.Tweet>
{
    public void Configure(EntityTypeBuilder<Models.Tweet> builder)
    {
        builder.HasKey(x => x.Id);
        builder.HasIndex(x => x.Id);

        builder.Property(x => x.Content)
            .HasColumnName("Content")
            .HasColumnType("nvarchar(240)")
            .IsRequired();

        builder.Property(x => x.DateCreated)
            .HasColumnName("DateCreated")
            .HasColumnType("nvarchar(64)")
            .HasDefaultValueSql("getUtcDate()")
            .IsRequired();
    }
}
