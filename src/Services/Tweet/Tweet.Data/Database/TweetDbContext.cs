﻿using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Tweet.Data.Database
{
    public class TweetDbContext : DbContext
    {
        public TweetDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Models.Tweet> Tweets { get; set; } = default!;
        public DbSet<Models.Profile> Profiles { get; set; } = default!;


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }
    }
}
