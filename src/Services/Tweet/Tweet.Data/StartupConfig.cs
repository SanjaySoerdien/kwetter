﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Tweet.Data.Infrastructure;

namespace Tweet.Data;
public static class StartupConfig
{
    public static IServiceCollection AddTweetData(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddServices()
            .AddEfCore(configuration)
            .AddRabbitMQMassTransit(configuration);

        return services;
    }
}
