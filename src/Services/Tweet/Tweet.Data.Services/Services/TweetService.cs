﻿using MediatR;
using Tweet.Data.Services.Commands;
using Tweet.Data.Services.Interfaces;
using Tweet.Data.Services.Models.Tweet;
using Tweet.Data.Services.Queries;

namespace Tweet.Data.Services.Services
{
    public class TweetService : ITweetService
    {
        private readonly IMediator _mediator;

        public TweetService(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<IEnumerable<TweetDto>> GetAsync()
        {
            return await _mediator.Send(new GetTweetsQuery());
        }

        public async Task<TweetDto> GetByIdAsync(int id)
        {
            return await _mediator.Send(new GetTweetQuery(id));
        }

        public async Task<TweetDto> CreateAsync(TweetCreateDto model)
        {
            return await _mediator.Send(new CreateTweetCommand(model));
        }

        public async Task<TweetDto> UpdateByIdAsync(int id, TweetUpdateDto model)
        {
            return await _mediator.Send(new UpdateTweetCommand(id, model));
        }

        public async Task<bool> DeleteByIdAsync(int id)
        {
            return await _mediator.Send(new DeleteTweetCommand(id));
        }
    }
}
