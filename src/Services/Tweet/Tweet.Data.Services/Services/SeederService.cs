﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Tweet.Data.Database;
using Tweet.Data.Models;
using Tweet.Data.Services.Interfaces;
using TweetModel = Tweet.Data.Models.Tweet;


namespace Tweet.Data.Services.Services
{
    public class SeederService : ISeederService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public SeederService(IServiceProvider serviceProvider, IWebHostEnvironment webHostEnvironment)
        {
            _serviceProvider = serviceProvider.CreateScope().ServiceProvider;
            _webHostEnvironment = webHostEnvironment;
        }

        public async Task MigrateAsync()
        {
            var context = _serviceProvider.GetRequiredService<TweetDbContext>();
            await context.Database.MigrateAsync();
        }

        public async Task SeedAsync()
        {
            var context = _serviceProvider.GetRequiredService<TweetDbContext>();
            if (!context.Tweets.Any())
            {
                var profile1 = new Profile { ProfileId = Guid.Parse("9517795e-d74a-4a04-8a76-0e6310db8fd1"), Username = "JohnDoe", Usertag = "@JohnDoe" };
                var profile2 = new Profile { ProfileId = Guid.Parse("4f54eaf6-6c68-4225-aadc-1ecf3aaccaef"), Username = "JaneDoe", Usertag = "@JaneDoe" };
                var profile3 = new Profile { ProfileId = Guid.Parse("d65ea3be-b456-4dee-b360-6d0a267ef4b4"), Username = "JohnSmith", Usertag = "@JohnSmith" };
                await context.AddRangeAsync(profile1, profile2, profile3);
                context.AddRange(new List<TweetModel>()
                {
                    new TweetModel{ Profile = profile1, Content = "This it he first tweet ever how cool",},
                    new TweetModel{ Profile = profile1, Content = "this is the second"},
                    new TweetModel{ Profile = profile2, Content = "fontys is a school"},
                    new TweetModel{ Profile = profile2, Content = "i need sleep"},
                    new TweetModel{ Profile = profile2, Content = "Why do i do this"},
                    new TweetModel{ Profile = profile3, Content = "I like cats"},
                    new TweetModel{ Profile = profile3, Content = "the tweet of the century"},
                    new TweetModel{ Profile = profile3, Content = "wat are you doing?"}
                });
                await context.SaveChangesAsync();
            }
        }
    }
}
