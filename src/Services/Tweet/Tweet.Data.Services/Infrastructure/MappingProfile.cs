﻿using AutoMapper;
using Kwetter.Requests.Tweets.Responses.Model;
using Tweet.Data.Services.Models.Tweet;
using ProfileModel = Tweet.Data.Models.Profile;
using TweetModel = Tweet.Data.Models.Tweet;

namespace Tweet.Data.Services.Infrastructure;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<TweetModel, TweetDto>().IncludeMembers(t => t.Profile).ReverseMap();
        CreateMap<TweetModel, TweetEventModel>().IncludeMembers(t => t.Profile).ReverseMap();

        CreateMap<ProfileModel, TweetModel>().ReverseMap();

        CreateMap<ProfileModel, TweetEventModel>().ReverseMap();
        CreateMap<ProfileModel, TweetDto>().ReverseMap();

        CreateMap<TweetModel, TweetCreateDto>().ReverseMap();
        CreateMap<TweetModel, TweetUpdateDto>().ReverseMap();
    }
}
