﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Tweet.Data.Services.Commands
{
    public record DeleteTweetCommand([Required] int Id) : IRequest<bool>;
}
