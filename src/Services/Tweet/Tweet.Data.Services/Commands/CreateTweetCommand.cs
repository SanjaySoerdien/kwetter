﻿using MediatR;
using System.ComponentModel.DataAnnotations;
using Tweet.Data.Services.Models.Tweet;

namespace Tweet.Data.Services.Commands;

public record CreateTweetCommand([Required] TweetCreateDto Model) : IRequest<TweetDto>;
