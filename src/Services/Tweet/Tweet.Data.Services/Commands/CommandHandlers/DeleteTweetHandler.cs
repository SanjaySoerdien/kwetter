﻿using Kwetter.Events.TweetEvents;
using MassTransit;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Tweet.Data.Database;
using Tweet.Data.Services.Commands;

namespace Tweet.Data.Services.CommandHandlers
{
    public class DeleteTweetHandler : IRequestHandler<DeleteTweetCommand, bool>
    {
        private readonly TweetDbContext _context;
        private readonly IPublishEndpoint _publisher;

        public DeleteTweetHandler(TweetDbContext context, IPublishEndpoint publisher)
        {
            _context = context;
            _publisher = publisher;
        }

        public async Task<bool> Handle(DeleteTweetCommand request, CancellationToken cancellationToken)
        {
            var dbTweet = await _context.Tweets.FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken);
            if (dbTweet == null) return false;
            _context.Tweets.Remove(dbTweet);
            await _context.SaveChangesAsync(cancellationToken);
            await _publisher.Publish(new TweetDeletedEvent(dbTweet.Id), cancellationToken);
            return true;
        }
    }
}
