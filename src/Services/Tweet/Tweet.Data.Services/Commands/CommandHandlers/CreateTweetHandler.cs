﻿using AutoMapper;
using Kwetter.Events.TweetEvents;
using MassTransit;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Tweet.Data.Database;
using Tweet.Data.Services.Commands;
using Tweet.Data.Services.Models.Tweet;
using TweetModel = Tweet.Data.Models.Tweet;

namespace Tweet.Data.Services.CommandHandlers
{
    public class CreateTweetHandler : IRequestHandler<CreateTweetCommand, TweetDto?>
    {
        private readonly TweetDbContext _context;
        private readonly IMapper _mapper;
        private readonly IPublishEndpoint _publisher;

        public CreateTweetHandler(TweetDbContext context, IMapper mapper, IPublishEndpoint publisher)
        {
            _context = context;
            _mapper = mapper;
            _publisher = publisher;
        }

        public async Task<TweetDto?> Handle(CreateTweetCommand request, CancellationToken cancellationToken)
        {
            var profile = await _context.Profiles.Include(x => x.Tweets).FirstOrDefaultAsync(x => x.ProfileId == request.Model.UserId, cancellationToken);
            if (profile == null) return null;
            var tweetToAdd = _mapper.Map<TweetModel>(request.Model);
            profile.Tweets.Add(tweetToAdd);
            await _context.SaveChangesAsync(cancellationToken);
            await _publisher.Publish(new TweetCreatedEvent(tweetToAdd.Id), cancellationToken);

            return _mapper.Map<TweetDto>(tweetToAdd);
        }
    }
}
