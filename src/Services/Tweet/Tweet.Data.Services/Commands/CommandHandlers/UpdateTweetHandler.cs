﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Tweet.Data.Database;
using Tweet.Data.Services.Commands;
using Tweet.Data.Services.Models.Tweet;

namespace Tweet.Data.Services.CommandHandlers
{
    public class UpdateTweetHandler : IRequestHandler<UpdateTweetCommand, TweetDto>
    {
        private readonly TweetDbContext _context;
        private readonly IMapper _mapper;

        public UpdateTweetHandler(TweetDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<TweetDto> Handle(UpdateTweetCommand request, CancellationToken cancellationToken)
        {
            var dbTweet = await _context.Tweets.FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken);

            _mapper.Map(request.Model, dbTweet);
            await _context.SaveChangesAsync(cancellationToken);

            return _mapper.Map<TweetDto>(dbTweet);
        }
    }
}
