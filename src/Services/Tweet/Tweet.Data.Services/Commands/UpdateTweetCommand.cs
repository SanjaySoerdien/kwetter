﻿using MediatR;
using System.ComponentModel.DataAnnotations;
using Tweet.Data.Services.Models.Tweet;

namespace Tweet.Data.Services.Commands
{
    public record UpdateTweetCommand([Required] int Id, [Required] TweetUpdateDto Model) : IRequest<TweetDto>;
}
