﻿using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Tweet.Data.Services.Infrastructure;
using Tweet.Data.Services.Interfaces;
using Tweet.Data.Services.Services;

namespace Tweet.Data.Services;
public static class StartupConfig
{
    public static IServiceCollection AddTweetDataServices(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddServices()
            .AddMapper(configuration)
            .AddTweetData(configuration)
            .AddScoped<ISeederService, SeederService>()
            .AddMediatR(typeof(StartupConfig).Assembly);

        return services;
    }
}
