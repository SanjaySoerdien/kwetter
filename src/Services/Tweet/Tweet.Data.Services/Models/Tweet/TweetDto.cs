﻿using System.ComponentModel.DataAnnotations;

namespace Tweet.Data.Services.Models.Tweet;

public record TweetDto(

    [Required]
    int Id = default!,

    [Required]
    [MaxLength(64)]
    string ProfileUsername = default!,

    [Required]
    [MaxLength(64)]
    [RegularExpression(@"^\S*$", ErrorMessage = "No white space allowed")]
    string ProfileUsertag = default!,

    [Required]
    [MaxLength(240)]
    string Content = default!,

    [Required]
    int Likes = 0,
    [Required]
    DateTime DateCreated = default!
);
