﻿using System.ComponentModel.DataAnnotations;

namespace Tweet.Data.Services.Models.Tweet
{
    public record TweetCreateDto()
    {
        [Required]
        public Guid UserId { get; set; }

        [Required]
        [MaxLength(240)]
        public string Content { get; init; } = default!;
    }
}
