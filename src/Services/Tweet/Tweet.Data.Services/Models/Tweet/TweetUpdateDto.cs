﻿using System.ComponentModel.DataAnnotations;

namespace Tweet.Data.Services.Models.Tweet
{
    public record TweetUpdateDto(
        [Required]
        [MaxLength(240)]
        string Content = default!
    );
}