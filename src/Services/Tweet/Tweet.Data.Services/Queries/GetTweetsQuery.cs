﻿using MediatR;
using Tweet.Data.Services.Models.Tweet;

namespace Tweet.Data.Services.Queries
{
    public record GetTweetsQuery() : IRequest<IEnumerable<TweetDto>>;
}
