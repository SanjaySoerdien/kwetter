﻿using MediatR;
using System.ComponentModel.DataAnnotations;
using Tweet.Data.Services.Models.Tweet;

namespace Tweet.Data.Services.Queries
{
    public record GetTweetQuery([Required] int Id) : IRequest<TweetDto>;
}
