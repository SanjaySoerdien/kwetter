﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Tweet.Data.Database;
using Tweet.Data.Services.Models.Tweet;
using Tweet.Data.Services.Queries;

namespace Tweet.Data.Services.QueryHandlers
{
    public class GetTweetsHandler : IRequestHandler<GetTweetsQuery, IEnumerable<TweetDto>>
    {
        private readonly TweetDbContext _context;
        private readonly IMapper _mapper;

        public GetTweetsHandler(TweetDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TweetDto>> Handle(GetTweetsQuery request, CancellationToken cancellationToken)
        {
            var dbTweets = await _context.Tweets.Include(t => t.Profile).ToListAsync(cancellationToken);
            dbTweets.Reverse();
            return _mapper.Map<IEnumerable<TweetDto>>(dbTweets);
        }
    }
}
