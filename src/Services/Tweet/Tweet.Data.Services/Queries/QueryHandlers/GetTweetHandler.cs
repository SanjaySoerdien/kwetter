﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Tweet.Data.Database;
using Tweet.Data.Services.Models.Tweet;
using Tweet.Data.Services.Queries;

namespace Tweet.Data.Services.QueryHandlers
{
    public class GetTweetHandler : IRequestHandler<GetTweetQuery, TweetDto?>
    {
        private readonly TweetDbContext _context;
        private readonly IMapper _mapper;

        public GetTweetHandler(TweetDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<TweetDto?> Handle(GetTweetQuery request, CancellationToken cancellationToken)
        {
            var dbTweet = await _context.Tweets.Include(x => x.Profile).FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken);
            if (dbTweet == null) return null;
            return _mapper.Map<TweetDto>(dbTweet);
        }
    }
}
