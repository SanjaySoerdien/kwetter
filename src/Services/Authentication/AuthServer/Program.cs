using AuthServer.Infrastructure;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddServices();

var app = builder.Build();

app.ConfigureApp(builder.Environment);
app.MapGet("/", () => "Hello AuthServer!");

app.Run();
