﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace AuthServer.ViewModels;

public class LogoutViewModel
{
    [BindNever]
    public string? RequestId { get; set; }
}