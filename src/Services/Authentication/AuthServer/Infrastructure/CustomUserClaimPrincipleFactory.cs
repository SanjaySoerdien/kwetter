﻿using AuthServer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System.Security.Claims;

namespace AuthServer.Infrastructure
{
    public class CustomUserClaimPrincipleFactory : UserClaimsPrincipalFactory<ApplicationUser>
    {
        public CustomUserClaimPrincipleFactory(UserManager<ApplicationUser> userManager,
                                               IOptions<IdentityOptions> optionsAccessor) : base(userManager, optionsAccessor)
        {
        }

        public override async Task<ClaimsPrincipal> CreateAsync(ApplicationUser user)
        {
            var principle = await base.CreateAsync(user);

            return principle;
        }
    }
}
