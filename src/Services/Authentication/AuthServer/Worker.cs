﻿using AuthServer.Database;
using AuthServer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using OpenIddict.Abstractions;
using static OpenIddict.Abstractions.OpenIddictConstants;

namespace AuthServer;

public class Worker : IHostedService
{
    private readonly IServiceProvider _serviceProvider;
    private readonly string clientId = "kwetter-blazor";

    public Worker(IServiceProvider serviceProvider)
        => _serviceProvider = serviceProvider;

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        await using var scope = _serviceProvider.CreateAsyncScope();

        var context = scope.ServiceProvider.GetRequiredService<AuthDbContext>();
        await context.Database.EnsureCreatedAsync(cancellationToken);

        var manager = scope.ServiceProvider.GetRequiredService<IOpenIddictApplicationManager>();
        var userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();

        if (await manager.FindByClientIdAsync(clientId, cancellationToken) is null)
        {
            await manager.CreateAsync(new OpenIddictApplicationDescriptor
            {
                ClientId = clientId,
                ConsentType = ConsentTypes.Explicit,
                DisplayName = "Kwetter",
                Type = ClientTypes.Public,
                PostLogoutRedirectUris =
                {
                    new Uri("https://localhost:8080/authentication/logout-callback"),
                    new Uri("https://localhost:8080/")
                },
                RedirectUris =
                {
                    new Uri("https://localhost:8080/authentication/login-callback")
                },
                Permissions =
                {
                    Permissions.Endpoints.Authorization,
                    Permissions.Endpoints.Logout,
                    Permissions.Endpoints.Token,
                    Permissions.GrantTypes.AuthorizationCode,
                    Permissions.GrantTypes.RefreshToken,
                    Permissions.GrantTypes.ClientCredentials,
                    Permissions.ResponseTypes.Code,
                    Permissions.Scopes.Email,
                    Permissions.Scopes.Profile,
                    Permissions.Scopes.Roles
                },
                Requirements =
                {
                    Requirements.Features.ProofKeyForCodeExchange
                }
            }, cancellationToken);
        }

        if (!await userManager.Users.AnyAsync(cancellationToken))
        {
            var user1 = new ApplicationUser
            {
                Email = "testuser1@test.com",
                EmailConfirmed = true,
                UserName = "TestUser1"
            };
            var user2 = new ApplicationUser
            {
                Email = "testuser2@test.com",
                EmailConfirmed = true,
                UserName = "TestUser2"
            };
            var user3 = new ApplicationUser
            {
                Email = "testuser3@test.com",
                EmailConfirmed = true,
                UserName = "TestUser3"
            };
            await userManager.CreateAsync(user1, "Pass123$");
            await userManager.CreateAsync(user2, "Pass123$");
            await userManager.CreateAsync(user3, "Pass123$");
        }
    }

    public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
}