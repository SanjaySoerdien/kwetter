using Like.API.Infrastructure;
using Like.Data.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddLikeDataServices(builder.Configuration);

builder.Services.AddControllers();
builder.Services.AddServices(builder.Configuration)
    .AddSwagger();

var app = builder.Build()
                .InitApp();


app.Run();
