﻿using Like.Data.Services.Interfaces;
using Like.Data.Services.Models.Like;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Like.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class LikeController : ControllerBase
    {
        private readonly ILikeService _likeService;

        public LikeController(ILikeService likeService)
        {
            _likeService = likeService;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var likeDtos = await _likeService.GetAsync();

            return Ok(likeDtos);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetByIdAsync([FromRoute] int id)
        {
            var likeDto = await _likeService.GetByIdAsync(id);

            return Ok(likeDto);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] LikeCreateDto model)
        {
            var likeDto = await _likeService.CreateAsync(model);
            if (likeDto == null) return BadRequest("Unable to create like");
            return Created($"api/like/{likeDto.Id}", likeDto);
        }

        [AllowAnonymous]
        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteByIdAsync([FromRoute] int id)
        {
            var IsDeleted = await _likeService.DeleteByIdAsync(id);

            if (IsDeleted)
            {
                return NoContent();
            }

            return BadRequest("Unable to delete like");
        }
    }
}
