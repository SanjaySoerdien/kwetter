﻿using AutoMapper;
using Like.Data.Services.Models.Like;
using LikeModel = Like.Data.Models.Like;

namespace Like.Data.Services.Infrastructure
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<LikeModel, LikeDto>().ReverseMap();
            CreateMap<LikeModel, LikeCreateDto>().ReverseMap();
        }
    }
}
