﻿using System.ComponentModel.DataAnnotations;

namespace Like.Data.Services.Models.Like;

public record LikeDto()
{
    [Required]
    public int? Id { get; init; } = default;

    [Required]
    public Guid? UserId { get; init; } = default;

    [Required]
    public int? TweetId { get; init; } = default;
}

