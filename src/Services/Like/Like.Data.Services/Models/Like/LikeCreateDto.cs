﻿using System.ComponentModel.DataAnnotations;

namespace Like.Data.Services.Models.Like;

public record LikeCreateDto()
{
    [Required]
    public Guid? UserId { get; init; } = default;

    [Required]
    public int? TweetId { get; set; } = default;
}
