﻿using Like.Data.Database;
using Like.Data.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using LikeModel = Like.Data.Models.Like;


namespace Like.Data.Services.Services
{
    public class SeederService : ISeederService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public SeederService(IServiceProvider serviceProvider, IWebHostEnvironment webHostEnvironment)
        {
            _serviceProvider = serviceProvider.CreateScope().ServiceProvider;
            _webHostEnvironment = webHostEnvironment;
        }

        public async Task MigrateAsync()
        {
            var context = _serviceProvider.GetRequiredService<LikeDbContext>();
            await context.Database.MigrateAsync();
        }

        public async Task SeedAsync()
        {
            var context = _serviceProvider.GetRequiredService<LikeDbContext>();
            Guid guid1 = Guid.Parse("9517795e-d74a-4a04-8a76-0e6310db8fd1");
            Guid guid2 = Guid.Parse("4f54eaf6-6c68-4225-aadc-1ecf3aaccaef");
            Guid guid3 = Guid.Parse("d65ea3be-b456-4dee-b360-6d0a267ef4b4");

            if (!context.Likes.Any())
            {
                context.AddRange(new List<LikeModel>()
                {
                    new LikeModel{ TweetId = 1, UserId = guid1 },
                    new LikeModel{ TweetId = 1, UserId = guid2 },
                    new LikeModel{ TweetId = 1, UserId = guid3 },
                    new LikeModel{ TweetId = 2, UserId = guid2 },
                    new LikeModel{ TweetId = 2, UserId = guid1 },
                    new LikeModel{ TweetId = 3, UserId = guid3 },
                    new LikeModel{ TweetId = 3, UserId = guid2 },
                    new LikeModel{ TweetId = 3, UserId = guid1 }
                });
                await context.SaveChangesAsync();
            }
        }
    }
}
