﻿using Like.Data.Services.Commands;
using Like.Data.Services.Interfaces;
using Like.Data.Services.Models.Like;
using Like.Data.Services.Queries;
using MediatR;

namespace Like.Data.Services.Services
{
    public class LikeService : ILikeService
    {
        private readonly IMediator _mediator;

        public LikeService(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<IEnumerable<LikeDto>> GetAsync()
        {
            return await _mediator.Send(new GetLikesQuery());
        }

        public async Task<LikeDto> GetByIdAsync(int id)
        {
            return await _mediator.Send(new GetLikeQuery(id));
        }

        public async Task<LikeDto?> CreateAsync(LikeCreateDto model)
        {
            return await _mediator.Send(new CreateLikeCommand(model));
        }

        public async Task<bool> DeleteByIdAsync(int id)
        {
            return await _mediator.Send(new DeleteLikeCommand(id));
        }
    }
}
