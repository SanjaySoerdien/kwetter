﻿using MediatR;
using System.ComponentModel.DataAnnotations;
using Like.Data.Services.Models.Like;

namespace Like.Data.Services.Queries
{
    public record GetLikeQuery([Required] int Id) : IRequest<LikeDto>;
}
