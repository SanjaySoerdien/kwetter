﻿using AutoMapper;
using Like.Data.Database;
using Like.Data.Services.Models.Like;
using Like.Data.Services.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Like.Data.Services.QueryHandlers
{
    public class GetLikesHandler : IRequestHandler<GetLikesQuery, IEnumerable<LikeDto>>
    {
        private readonly LikeDbContext _context;
        private readonly IMapper _mapper;

        public GetLikesHandler(LikeDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<LikeDto>> Handle(GetLikesQuery request, CancellationToken cancellationToken)
        {
            var dbLikes = await _context.Likes.ToListAsync(cancellationToken);
            dbLikes.Reverse();
            return _mapper.Map<IEnumerable<LikeDto>>(dbLikes);
        }
    }
}
