﻿using AutoMapper;
using Like.Data.Database;
using Like.Data.Services.Models.Like;
using Like.Data.Services.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Like.Data.Services.QueryHandlers
{
    public class GetLikeHandler : IRequestHandler<GetLikeQuery, LikeDto>
    {
        private readonly LikeDbContext _context;
        private readonly IMapper _mapper;

        public GetLikeHandler(LikeDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<LikeDto> Handle(GetLikeQuery request, CancellationToken cancellationToken)
        {
            var dbLike = await _context.Likes.FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken);
            return _mapper.Map<LikeDto>(dbLike);
        }
    }
}
