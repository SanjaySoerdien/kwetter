﻿using MediatR;
using Like.Data.Services.Models.Like;

namespace Like.Data.Services.Queries
{
    public record GetLikesQuery() : IRequest<IEnumerable<LikeDto>>;
}
