﻿using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Like.Data.Services.Infrastructure;
using Like.Data.Services.Interfaces;
using Like.Data.Services.Services;

namespace Like.Data.Services;
public static class StartupConfig
{
    public static IServiceCollection AddLikeDataServices(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddServices()
            .AddMapper(configuration)
            .AddLikeData(configuration)
            .AddScoped<ISeederService, SeederService>()
            .AddMediatR(typeof(StartupConfig).Assembly);

        return services;
    }
}
