﻿namespace Like.Data.Services.Interfaces
{
    public interface ISeederService
    {
        public Task MigrateAsync();
        public Task SeedAsync();
    }
}
