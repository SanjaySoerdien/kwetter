﻿using Like.Data.Services.Models.Like;

namespace Like.Data.Services.Interfaces
{
    public interface ILikeService
    {
        Task<IEnumerable<LikeDto>> GetAsync();
        Task<LikeDto> GetByIdAsync(int id);
        Task<LikeDto?> CreateAsync(LikeCreateDto model);
        Task<bool> DeleteByIdAsync(int id);
    }
}
