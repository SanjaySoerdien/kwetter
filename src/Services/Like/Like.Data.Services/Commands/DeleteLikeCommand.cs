﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Like.Data.Services.Commands
{
    public record DeleteLikeCommand([Required] int Id) : IRequest<bool>;
}
