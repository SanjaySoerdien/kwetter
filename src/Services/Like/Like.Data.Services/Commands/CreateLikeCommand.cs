﻿using Like.Data.Services.Models.Like;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Like.Data.Services.Commands
{
    public record CreateLikeCommand([Required] LikeCreateDto Model) : IRequest<LikeDto?>;
}
