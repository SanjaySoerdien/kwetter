﻿using AutoMapper;
using Kwetter.Events.LikeEvents;
using Like.Data.Database;
using Like.Data.Services.Commands;
using Like.Data.Services.Models.Like;
using MassTransit;
using MediatR;
using Microsoft.EntityFrameworkCore;
using LikeModel = Like.Data.Models.Like;

namespace Like.Data.Services.CommandHandlers
{
    public class CreateLikeHandler : IRequestHandler<CreateLikeCommand, LikeDto?>
    {
        private readonly LikeDbContext _context;
        private readonly IMapper _mapper;
        private readonly IPublishEndpoint _publisher;

        public CreateLikeHandler(LikeDbContext context, IMapper mapper, IPublishEndpoint publisher)
        {
            _context = context;
            _mapper = mapper;
            _publisher = publisher;
        }

        public async Task<LikeDto?> Handle(CreateLikeCommand request, CancellationToken cancellationToken)
        {
            var likeToAdd = _mapper.Map<LikeModel>(request.Model);

            if (await _context.Likes.AnyAsync(x => x.UserId == likeToAdd.UserId && x.TweetId == likeToAdd.TweetId, cancellationToken)) return null;
            await _context.AddAsync(likeToAdd, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            await _publisher.Publish(new LikeCreatedEvent(likeToAdd.Id), cancellationToken);

            return _mapper.Map<LikeDto>(likeToAdd);
        }
    }
}
