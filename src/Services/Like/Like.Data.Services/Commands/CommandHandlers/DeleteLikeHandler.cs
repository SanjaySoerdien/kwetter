﻿using Kwetter.Events.LikeEvents;
using Like.Data.Database;
using Like.Data.Services.Commands;
using MassTransit;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Like.Data.Services.CommandHandlers
{
    public class DeleteLikeHandler : IRequestHandler<DeleteLikeCommand, bool>
    {
        private readonly LikeDbContext _context;
        private readonly IPublishEndpoint _publisher;

        public DeleteLikeHandler(LikeDbContext context, IPublishEndpoint publisher)
        {
            _context = context;
            _publisher = publisher;
        }

        public async Task<bool> Handle(DeleteLikeCommand request, CancellationToken cancellationToken)
        {
            var dbLike = await _context.Likes.FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken);
            if (dbLike == null) return false;
            _context.Likes.Remove(dbLike);
            await _context.SaveChangesAsync(cancellationToken);
            await _publisher.Publish(new LikeDeletedEvent(dbLike.Id), cancellationToken);
            return true;
        }
    }
}
