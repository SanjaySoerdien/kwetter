﻿using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Like.Data.Database
{
    public class LikeDbContext : DbContext
    {
        public LikeDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Models.Like> Likes { get; set; } = default!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }
    }
}
