﻿namespace Like.Data.Models;

public class Like
{
    public int Id { get; set; } = default!;
    public Guid UserId { get; set; }
    public int TweetId { get; set; } = default!;
}
