﻿using Kwetter.Common;
using Like.Data.Database;
using Like.Data.Markers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Like.Data.Infrastructure
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            return services;
        }

        public static IServiceCollection AddEfCore(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<LikeDbContext>(config =>
            {
                config.UseSqlServer(configuration.GetConnectionString("like-db"),
                sqlServerOptionsAction: sqlOptions =>
                {
                    sqlOptions.EnableRetryOnFailure();
                });
            });
            return services;
        }

        public static IServiceCollection AddRabbitMQMassTransit(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddRabbitMQMassTransitWithConsumers<IAssemblyMarker>(configuration["RabbitMQSettings:Host"], configuration["ServiceSettings:ServiceName"]);
            return services;
        }
    }
}
