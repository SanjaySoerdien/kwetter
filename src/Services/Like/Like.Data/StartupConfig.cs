﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Like.Data.Infrastructure;

namespace Like.Data;
public static class StartupConfig
{
    public static IServiceCollection AddLikeData(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddServices()
            .AddEfCore(configuration)
            .AddRabbitMQMassTransit(configuration);

        return services;
    }
}
