﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Like.Data.Configuration
{
    public class LikeConfiguration : IEntityTypeConfiguration<Models.Like>
    {
        public void Configure(EntityTypeBuilder<Models.Like> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => x.Id);
        }
    }
}
