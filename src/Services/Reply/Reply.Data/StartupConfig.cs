﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Reply.Data.Infrastructure;

namespace Reply.Data;
public static class StartupConfig
{
    public static IServiceCollection AddReplyData(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddServices()
            .AddEfCore(configuration)
            .AddRabbitMQMassTransit(configuration);

        return services;
    }
}
