﻿namespace Reply.Data.Models
{
    public class Reply
    {
        public int Id { get; set; } = default!;
        public string Username { get; set; } = default!;
        public string Usertag { get; set; } = default!;
        public string Content { get; set; } = default!;
        public DateTime DateCreated { get; set; } = DateTime.Now;

        public virtual Tweet Tweet { get; set; } = default!;
    }
}
