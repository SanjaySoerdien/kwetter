﻿namespace Reply.Data.Models
{
    public class Tweet
    {
        public int Id { get; set; }
        public int TweetId { get; set; }

        public virtual ICollection<Reply> Replies { get; set; } = new List<Reply>();
    }
}
