﻿using Kwetter.Events.TweetEvents;
using MassTransit;
using Microsoft.EntityFrameworkCore;

namespace Reply.Data.Consumers;

public class TweetDeletedEventConsumer : IConsumer<TweetDeletedEvent>
{
    private readonly ReplyDbContext _context;

    public TweetDeletedEventConsumer(ReplyDbContext context)
    {
        _context = context;
    }

    public async Task Consume(ConsumeContext<TweetDeletedEvent> context)
    {
        var dbTweet = await _context.Tweets.FirstOrDefaultAsync(t => t.Id == context.Message.Id);
        if (dbTweet == null) return;
        _context.Remove(dbTweet);
        await _context.SaveChangesAsync();
    }
}
