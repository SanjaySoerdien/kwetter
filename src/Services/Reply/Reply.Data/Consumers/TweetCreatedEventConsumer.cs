﻿using Kwetter.Events.TweetEvents;
using MassTransit;
using Reply.Data.Models;

namespace Reply.Data.Consumers;

public class TweetCreatedEventConsumer : IConsumer<TweetCreatedEvent>
{
    private readonly ReplyDbContext _context;

    public TweetCreatedEventConsumer(ReplyDbContext context)
    {
        _context = context;
    }

    public async Task Consume(ConsumeContext<TweetCreatedEvent> context)
    {
        await _context.AddAsync(new Tweet { TweetId = context.Message.Id });
        await _context.SaveChangesAsync();
    }
}
