﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Reply.Data.Configuration
{
    public class ReplyConfiguration : IEntityTypeConfiguration<Models.Reply>
    {
        public void Configure(EntityTypeBuilder<Models.Reply> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => x.Id);

            builder.Property(x => x.Username)
                .HasMaxLength(64)
                .HasColumnName("Username")
                .HasColumnType("nvarchar(240)")
                .IsRequired();

            builder.Property(x => x.Usertag)
                .HasMaxLength(64)
                .HasColumnName("Usertag")
                .HasColumnType("nvarchar(240)")
                .IsRequired();

            builder.Property(x => x.Content)
                .HasColumnName("Content")
                .HasColumnType("nvarchar(240)")
                .IsRequired();

            builder.Property(x => x.DateCreated)
                .HasColumnName("DateCreated")
                .HasColumnType("nvarchar(64)")
                .HasDefaultValueSql("getUtcDate()")
                .IsRequired();
        }
    }
}
