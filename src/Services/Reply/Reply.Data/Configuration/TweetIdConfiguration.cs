﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reply.Data.Models;

namespace Reply.Data.Configuration
{
    public class TweetIdConfiguration : IEntityTypeConfiguration<Tweet>
    {
        public void Configure(EntityTypeBuilder<Tweet> builder)
        {
            builder.HasKey(t => t.Id);
            builder.HasIndex(t => t.Id);
            builder.HasIndex(t => t.TweetId).IsUnique();
        }
    }
}
