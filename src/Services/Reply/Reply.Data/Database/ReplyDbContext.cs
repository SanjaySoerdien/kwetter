﻿using Microsoft.EntityFrameworkCore;
using Reply.Data.Models;
using System.Reflection;

namespace Reply.Data
{
    public class ReplyDbContext : DbContext
    {
        public ReplyDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Models.Reply> Replies { get; set; } = default!;
        public DbSet<Tweet> Tweets { get; set; } = default!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }
    }
}


