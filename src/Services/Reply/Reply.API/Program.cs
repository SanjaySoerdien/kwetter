using Reply.API.Infrastructure;
using Reply.Data.Service;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddReplyDataServices(builder.Configuration);

builder.Services.AddControllers();
builder.Services.AddServices(builder.Configuration)
    .AddSwagger();

var app = builder.Build()
                .InitApp();

app.Run();
