﻿using Microsoft.AspNetCore.Mvc;
using Reply.Data.Service.Interfaces;
using Reply.Data.Service.Models;

namespace Reply.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReplyController : ControllerBase
    {
        private readonly IReplyService _replyService;

        public ReplyController(IReplyService replyService)
        {
            _replyService = replyService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var replyDtos = await _replyService.GetAsync();

            return Ok(replyDtos);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetByIdAsync([FromRoute] int id)
        {
            var replyDto = await _replyService.GetByIdAsync(id);

            return Ok(replyDto);
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] ReplyCreateDto model)
        {

            var replyDto = await _replyService.CreateAsync(model);
            if (replyDto is null) return BadRequest("Unable to create reply");
            return Created($"api/reply/{replyDto.Id}", replyDto);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateByIdAsync([FromRoute] int id, [FromBody] ReplyUpdateDto model)
        {
            var replyDto = await _replyService.UpdateByIdAsync(id, model);

            if (replyDto is null) return BadRequest("Unable to update reply");

            return Ok(replyDto);
        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteByIdAsync([FromRoute] int id)
        {
            var IsDeleted = await _replyService.DeleteByIdAsync(id);

            if (IsDeleted)
            {
                return NoContent();
            }

            return BadRequest("Unable to delete reply");
        }
    }
}

