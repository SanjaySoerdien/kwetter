﻿using MediatR;
using Reply.Data.Service.Models;
using System.ComponentModel.DataAnnotations;

namespace Reply.Data.Service.Queries
{
    public record GetReplyQuery([Required] int Id) : IRequest<ReplyDto>;
}
