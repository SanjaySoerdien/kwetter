﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Reply.Data.Service.Models;

namespace Reply.Data.Service.Queries.QueryHandlers
{
    public class GetReplyHandler : IRequestHandler<GetReplyQuery, ReplyDto>
    {
        private readonly ReplyDbContext _context;
        private readonly IMapper _mapper;

        public GetReplyHandler(ReplyDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ReplyDto> Handle(GetReplyQuery request, CancellationToken cancellationToken)
        {
            var dbReply = await _context.Replies.FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken);
            return _mapper.Map<ReplyDto>(dbReply);
        }
    }
}
