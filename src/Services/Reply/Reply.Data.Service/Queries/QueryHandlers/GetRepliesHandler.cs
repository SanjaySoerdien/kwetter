﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Reply.Data.Service.Models;

namespace Reply.Data.Service.Queries.QueryHandlers
{
    public class GetRepliesHandler : IRequestHandler<GetRepliesQuery, IEnumerable<ReplyDto>>
    {
        private readonly ReplyDbContext _context;
        private readonly IMapper _mapper;

        public GetRepliesHandler(ReplyDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ReplyDto>> Handle(GetRepliesQuery request, CancellationToken cancellationToken)
        {
            var dbTweets = await _context.Replies.ToListAsync(cancellationToken);
            return _mapper.Map<IEnumerable<ReplyDto>>(dbTweets);
        }
    }
}
