﻿using MediatR;
using Reply.Data.Service.Models;

namespace Reply.Data.Service.Queries
{
    public record GetRepliesQuery() : IRequest<IEnumerable<ReplyDto>>;
}
