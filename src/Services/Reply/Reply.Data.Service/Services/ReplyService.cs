﻿using MediatR;
using Reply.Data.Service.Commands;
using Reply.Data.Service.Interfaces;
using Reply.Data.Service.Models;
using Reply.Data.Service.Queries;

namespace Reply.Data.Service.Services
{
    public class ReplyService : IReplyService
    {
        private readonly IMediator _mediator;

        public ReplyService(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<IEnumerable<ReplyDto>> GetAsync()
        {
            return await _mediator.Send(new GetRepliesQuery());
        }

        public async Task<ReplyDto> GetByIdAsync(int id)
        {
            return await _mediator.Send(new GetReplyQuery(id));
        }

        public async Task<ReplyDto> CreateAsync(ReplyCreateDto model)
        {
            return await _mediator.Send(new CreateReplyCommand(model));
        }

        public async Task<ReplyDto> UpdateByIdAsync(int id, ReplyUpdateDto model)
        {
            return await _mediator.Send(new UpdateReplyCommand(id, model));
        }

        public async Task<bool> DeleteByIdAsync(int id)
        {
            return await _mediator.Send(new DeleteReplyCommand(id));
        }
    }
}
