﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Reply.Data.Models;
using Reply.Data.Service.Interfaces;
using ReplyModel = Reply.Data.Models.Reply;

namespace Reply.Data.Service.Services
{
    public class SeederService : ISeederService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public SeederService(IServiceProvider serviceProvider, IWebHostEnvironment webHostEnvironment)
        {
            _serviceProvider = serviceProvider.CreateScope().ServiceProvider;
            _webHostEnvironment = webHostEnvironment;
        }

        public async Task MigrateAsync()
        {
            var context = _serviceProvider.GetRequiredService<ReplyDbContext>();
            await context.Database.MigrateAsync();
        }

        public async Task SeedAsync()
        {
            var context = _serviceProvider.GetRequiredService<ReplyDbContext>();
            if (!context.Replies.Any())
            {
                Tweet tweet1 = new Tweet { TweetId = 1 };
                Tweet tweet2 = new Tweet { TweetId = 2 };
                Tweet tweet3 = new Tweet { TweetId = 3 };
                Tweet tweet4 = new Tweet { TweetId = 4 };

                context.Add(tweet1);
                context.Add(tweet2);
                context.Add(tweet3);
                context.Add(tweet4);

                context.Replies.AddRange(new List<ReplyModel>()
                {
                    new ReplyModel{ Tweet = tweet1, Usertag = "@JohnDoe", Username = "John Doe", Content = "What a nice tweet"},
                    new ReplyModel{ Tweet = tweet1, Usertag = "@JaneDoe", Username = "Jane Doe", Content = "Kwetter is very cool"},
                    new ReplyModel{ Tweet = tweet1, Usertag = "@JohnSmith", Username = "John Smith", Content = "What am i doing"},
                    new ReplyModel{ Tweet = tweet2, Usertag = "@JohnDoe", Username = "John doe", Content = "what do you meaaan?"},
                    new ReplyModel{ Tweet = tweet2, Usertag = "@RichardRoe", Username = "Richard Roe", Content = "fight me"},
                    new ReplyModel{ Tweet = tweet2, Usertag = "@MrBrown", Username = "Mr Brown", Content = "I like puppies"},
                    new ReplyModel{ Tweet = tweet3, Usertag = "@AverageJoe", Username = "averagejoe", Content = "What am i doing here"},
                    new ReplyModel{ Tweet = tweet3, Usertag = "@xxUser", Username = "userxx", Content = "Does this work?"}
                });
                await context.SaveChangesAsync();
            }
        }
    }
}