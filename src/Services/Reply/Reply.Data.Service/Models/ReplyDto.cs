﻿using System.ComponentModel.DataAnnotations;

namespace Reply.Data.Service.Models
{
    public record ReplyDto(

        [Required]
        int Id = default!,

        [Required]
        [MaxLength(64)]
        string Username = default!,

        [Required]
        [MaxLength(64)]
        string Usertag = default!,

        [Required]
        [MaxLength(240)]
        string Content = default!,
        [Required]
        DateTime DateCreated = default!
    );
}
