﻿using System.ComponentModel.DataAnnotations;

namespace Reply.Data.Service.Models
{
    public record ReplyUpdateDto
    {
        [Required]
        [MaxLength(240)]
        public string Content { get; init; } = default!;
    }
}
