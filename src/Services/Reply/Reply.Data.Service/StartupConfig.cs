﻿using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Reply.Data.Service.Infrastructure;
using Reply.Data.Service.Interfaces;
using Reply.Data.Service.Services;

namespace Reply.Data.Service;
public static class StartupConfig
{
    public static IServiceCollection AddReplyDataServices(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddServices()
            .AddMapper(configuration)
            .AddReplyData(configuration)
            .AddScoped<ISeederService, SeederService>()
            .AddMediatR(typeof(StartupConfig).Assembly);

        return services;
    }
}