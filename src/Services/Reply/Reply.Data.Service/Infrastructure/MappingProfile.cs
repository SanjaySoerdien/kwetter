﻿using AutoMapper;
using Reply.Data.Service.Models;
using ReplyModel = Reply.Data.Models.Reply;

namespace Reply.Data.Service.Infrastructure
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ReplyModel, ReplyDto>().ReverseMap();
            CreateMap<ReplyModel, ReplyCreateDto>().ReverseMap();
            CreateMap<ReplyModel, ReplyUpdateDto>().ReverseMap();
        }
    }
}
