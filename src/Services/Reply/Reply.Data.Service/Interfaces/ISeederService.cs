﻿namespace Reply.Data.Service.Interfaces
{
    public interface ISeederService
    {
        public Task MigrateAsync();
        public Task SeedAsync();
    }
}
