﻿using Reply.Data.Service.Models;

namespace Reply.Data.Service.Interfaces
{
    public interface IReplyService
    {
        Task<IEnumerable<ReplyDto>> GetAsync();
        Task<ReplyDto> GetByIdAsync(int id);
        Task<ReplyDto> CreateAsync(ReplyCreateDto model);
        Task<ReplyDto> UpdateByIdAsync(int id, ReplyUpdateDto model);
        Task<bool> DeleteByIdAsync(int id);
    }
}
