﻿using MediatR;
using Reply.Data.Service.Models;
using System.ComponentModel.DataAnnotations;

namespace Reply.Data.Service.Commands
{
    public record CreateReplyCommand([Required] ReplyCreateDto Model) : IRequest<ReplyDto>;
}
