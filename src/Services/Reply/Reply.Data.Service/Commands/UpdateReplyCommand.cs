﻿using MediatR;
using Reply.Data.Service.Models;
using System.ComponentModel.DataAnnotations;

namespace Reply.Data.Service.Commands
{
    public record UpdateReplyCommand([Required] int Id, [Required] ReplyUpdateDto Model) : IRequest<ReplyDto>;
}
