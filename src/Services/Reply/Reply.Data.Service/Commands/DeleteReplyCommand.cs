﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Reply.Data.Service.Commands
{
    public record DeleteReplyCommand([Required] int Id) : IRequest<bool>;
}
