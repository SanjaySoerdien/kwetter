﻿using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Reply.Data.Service.Commands.CommandHandlers
{
    public class DeleteReplyHandler : IRequestHandler<DeleteReplyCommand, bool>
    {
        private readonly ReplyDbContext _context;

        public DeleteReplyHandler(ReplyDbContext context)
        {
            _context = context;
        }

        public async Task<bool> Handle(DeleteReplyCommand request, CancellationToken cancellationToken)
        {
            var dbReply = await _context.Replies.FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken);
            if (dbReply == null) return false;
            _context.Replies.Remove(dbReply);
            await _context.SaveChangesAsync(cancellationToken);

            return true;
        }
    }
}
