﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Reply.Data.Service.Models;
using ReplyModel = Reply.Data.Models.Reply;

namespace Reply.Data.Service.Commands.CommandHandlers
{
    public class CreateReplyHandler : IRequestHandler<CreateReplyCommand, ReplyDto?>
    {
        private readonly ReplyDbContext _context;
        private readonly IMapper _mapper;

        public CreateReplyHandler(ReplyDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ReplyDto?> Handle(CreateReplyCommand request, CancellationToken cancellationToken)
        {
            var ReplyToAdd = _mapper.Map<ReplyModel>(request.Model);
            var tweetToAddReplyTo = await _context.Tweets.FirstOrDefaultAsync(t => t.Id == request.Model.TweetId, cancellationToken);
            if (tweetToAddReplyTo is not null)
            {
                tweetToAddReplyTo.Replies.Add(ReplyToAdd);
                await _context.SaveChangesAsync(cancellationToken);

                return _mapper.Map<ReplyDto>(ReplyToAdd);
            }
            return null;
        }
    }
}
