﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Reply.Data.Service.Models;

namespace Reply.Data.Service.Commands.CommandHandlers
{
    public class UpdateReplyHandler : IRequestHandler<UpdateReplyCommand, ReplyDto?>
    {
        private readonly ReplyDbContext _context;
        private readonly IMapper _mapper;

        public UpdateReplyHandler(ReplyDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public async Task<ReplyDto?> Handle(UpdateReplyCommand request, CancellationToken cancellationToken)
        {
            var dbReply = await _context.Replies.FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken);
            if (dbReply is null) return null;

            _mapper.Map(request.Model, dbReply);
            await _context.SaveChangesAsync(cancellationToken);

            return _mapper.Map<ReplyDto>(dbReply);
        }
    }
}
