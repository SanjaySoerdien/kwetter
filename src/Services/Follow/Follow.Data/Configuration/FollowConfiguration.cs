﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Follow.Data.Configuration
{
    public class FollowConfiguration : IEntityTypeConfiguration<Models.Follow>
    {
        public void Configure(EntityTypeBuilder<Models.Follow> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => x.Id);
        }
    }
}
