﻿namespace Follow.Data.Models;

public class Follow
{
    public int Id { get; set; } = default!;
    public Guid FollowerId { get; set; }
    public Guid FollowedId { get; set; }
}
