﻿using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Follow.Data.Database
{
    public class FollowDbContext : DbContext
    {
        public FollowDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Models.Follow> Follows { get; set; } = default!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }
    }
}
