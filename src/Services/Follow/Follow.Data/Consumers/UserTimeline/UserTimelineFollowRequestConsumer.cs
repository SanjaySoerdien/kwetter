﻿using Follow.Data.Database;
using Kwetter.Requests.Follows.Requests;
using Kwetter.Requests.Follows.Responses;
using MassTransit;
using Microsoft.EntityFrameworkCore;

namespace Follow.Data.Consumers.UserTimeline;

public class UserTimelineFollowRequestConsumer : IConsumer<UserTimelineFollowsRequest>
{
    private readonly FollowDbContext _dbContext;

    public UserTimelineFollowRequestConsumer(FollowDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task Consume(ConsumeContext<UserTimelineFollowsRequest> context)
    {
        var follows = await _dbContext.Follows
            .Where(f => f.FollowerId == context.Message.UserId)
            .Select(f => f.FollowedId)
            .ToListAsync();

        var response = new UserTimelineFollowsResponse();
        response.FollowIds = follows;

        await context.RespondAsync(response);
    }
}
