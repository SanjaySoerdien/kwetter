﻿using Follow.Data.Database;
using Kwetter.Events.FollowEvents;
using Kwetter.Events.ProfileEvents;
using MassTransit;
using Microsoft.EntityFrameworkCore;

namespace Follow.Data.Consumers;

public class ProfileDeletedEventConsumer : IConsumer<ProfileDeletedEvent>
{
    private readonly FollowDbContext _context;
    private readonly IPublishEndpoint _publishEndpoint;

    public ProfileDeletedEventConsumer(FollowDbContext context, IPublishEndpoint publishEndpoint)
    {
        _context = context;
        _publishEndpoint = publishEndpoint;
    }

    public async Task Consume(ConsumeContext<ProfileDeletedEvent> context)
    {
        var dbFollows = await _context.Follows.Where(t => t.FollowerId == context.Message.Id || t.FollowedId == context.Message.Id).ToListAsync();
        if (dbFollows == null) return;
        _context.RemoveRange(dbFollows);
        foreach (var follow in dbFollows)
        {
            await _publishEndpoint.Publish(new FollowDeletedEvent(FollowedId: follow.FollowedId, FollowerId: follow.FollowerId));
        }
        await _context.SaveChangesAsync();
    }
}
