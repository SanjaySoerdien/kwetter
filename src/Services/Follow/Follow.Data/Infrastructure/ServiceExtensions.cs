﻿using Follow.Data.Database;
using Follow.Data.Markers;
using Kwetter.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Follow.Data.Infrastructure
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            return services;
        }

        public static IServiceCollection AddEfCore(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<FollowDbContext>(config =>
            {
                config.UseSqlServer(configuration.GetConnectionString("follow-db"),
                sqlServerOptionsAction: sqlOptions =>
                {
                    sqlOptions.EnableRetryOnFailure();
                });
            });
            return services;
        }

        public static IServiceCollection AddRabbitMQMassTransit(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddRabbitMQMassTransitWithConsumers<IAssemblyMarker>(configuration["RabbitMQSettings:Host"], configuration["ServiceSettings:ServiceName"]);
            return services;
        }
    }
}
