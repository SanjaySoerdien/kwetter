﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Follow.Data.Infrastructure;

namespace Follow.Data;
public static class StartupConfig
{
    public static IServiceCollection AddFollowData(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddServices()
            .AddEfCore(configuration)
            .AddRabbitMQMassTransit(configuration);

        return services;
    }
}
