﻿using Follow.Data.Services.Interfaces;

namespace Follow.API.Infrastructure
{
    public class Seeder : IHostedService
    {
        private readonly ISeederService _seedService;

        public Seeder(IServiceProvider serviceProvider)
        {
            _seedService = serviceProvider.CreateScope().ServiceProvider.GetRequiredService<ISeederService>();
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await _seedService.MigrateAsync();
            await _seedService.SeedAsync();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
