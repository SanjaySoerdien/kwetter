﻿using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace Follow.API.Authentication
{
    public static class Policies
    {
        public const string AdminPolicy = "AdminPolicy";


        public static AuthorizationOptions ConfigurePolicies(this AuthorizationOptions options)
        {
            options.DefaultPolicy = DefaultPolicyBuilder();
            options.AddPolicy(AdminPolicy, AdminPolicyBuilder());

            return options;
        }

        private static AuthorizationPolicy DefaultPolicyBuilder() =>
            new AuthorizationPolicyBuilder()
                .RequireAuthenticatedUser()
                .Build();

        private static AuthorizationPolicy AdminPolicyBuilder() =>
            new AuthorizationPolicyBuilder()
                .RequireAuthenticatedUser()
                .RequireAssertion(x => IsAdmin(x.User))
                .Build();

        private static bool IsAdmin(ClaimsPrincipal user)
        {
            return user.HasClaim(x => x.Type == "admin");
        }
    }
}
