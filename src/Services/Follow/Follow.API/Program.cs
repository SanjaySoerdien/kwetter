using Follow.API.Infrastructure;
using Follow.Data.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddFollowDataServices(builder.Configuration);

builder.Services.AddControllers();
builder.Services.AddServices(builder.Configuration)
    .AddSwagger();

var app = builder.Build()
                .InitApp();


app.Run();
