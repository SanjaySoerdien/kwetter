﻿using Follow.Data.Services.Interfaces;
using Follow.Data.Services.Models.Follow;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Follow.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class FollowController : ControllerBase
    {
        private readonly IFollowService _followService;

        public FollowController(IFollowService followService)
        {
            _followService = followService;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var followDtos = await _followService.GetAsync();

            return Ok(followDtos);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetByIdAsync([FromRoute] int id)
        {
            var followDto = await _followService.GetByIdAsync(id);

            return Ok(followDto);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] FollowCreateDto model)
        {
            var followDto = await _followService.CreateAsync(model);
            if (followDto == null) return BadRequest("Unable to create follow");
            return Created($"api/follow/{followDto.Id}", followDto);
        }

        [AllowAnonymous]
        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteByIdAsync([FromRoute] int id)
        {
            var IsDeleted = await _followService.DeleteByIdAsync(id);

            if (IsDeleted)
            {
                return NoContent();
            }

            return BadRequest("Unable to delete follow");
        }
    }
}
