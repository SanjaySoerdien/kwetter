﻿using Follow.Data.Services.Commands;
using Follow.Data.Services.Interfaces;
using Follow.Data.Services.Models.Follow;
using Follow.Data.Services.Queries;
using MediatR;

namespace Follow.Data.Services.Services
{
    public class FollowService : IFollowService
    {
        private readonly IMediator _mediator;

        public FollowService(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<IEnumerable<FollowDto>> GetAsync()
        {
            return await _mediator.Send(new GetFollowsQuery());
        }

        public async Task<FollowDto> GetByIdAsync(int id)
        {
            return await _mediator.Send(new GetFollowQuery(id));
        }

        public async Task<FollowDto?> CreateAsync(FollowCreateDto model)
        {
            if (model.FollowerId == model.FollowedId) return null;
            return await _mediator.Send(new CreateFollowCommand(model));
        }

        public async Task<bool> DeleteByIdAsync(int id)
        {
            return await _mediator.Send(new DeleteFollowCommand(id));
        }
    }
}
