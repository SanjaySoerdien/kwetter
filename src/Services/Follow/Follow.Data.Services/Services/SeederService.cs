﻿using Follow.Data.Database;
using Follow.Data.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using FollowModel = Follow.Data.Models.Follow;


namespace Follow.Data.Services.Services
{
    public class SeederService : ISeederService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public SeederService(IServiceProvider serviceProvider, IWebHostEnvironment webHostEnvironment)
        {
            _serviceProvider = serviceProvider.CreateScope().ServiceProvider;
            _webHostEnvironment = webHostEnvironment;
        }

        public async Task MigrateAsync()
        {
            var context = _serviceProvider.GetRequiredService<FollowDbContext>();
            await context.Database.MigrateAsync();
        }

        public async Task SeedAsync()
        {
            var context = _serviceProvider.GetRequiredService<FollowDbContext>();
            var guid1 = Guid.Parse("9517795e-d74a-4a04-8a76-0e6310db8fd1");
            var guid2 = Guid.Parse("4f54eaf6-6c68-4225-aadc-1ecf3aaccaef");
            var guid3 = Guid.Parse("d65ea3be-b456-4dee-b360-6d0a267ef4b4");
            if (!context.Follows.Any())
            {
                context.AddRange(new List<FollowModel>()
                {
                    new FollowModel{ FollowerId = guid1, FollowedId = guid2 },
                    new FollowModel{ FollowerId = guid1, FollowedId = guid3 },
                    new FollowModel{ FollowerId = guid2, FollowedId = guid1 },
                    new FollowModel{ FollowerId = guid2, FollowedId = guid3 },
                    new FollowModel{ FollowerId = guid3, FollowedId = guid1 },
                    new FollowModel{ FollowerId = guid3, FollowedId = guid2 }
                });
                await context.SaveChangesAsync();
            }
        }
    }
}
