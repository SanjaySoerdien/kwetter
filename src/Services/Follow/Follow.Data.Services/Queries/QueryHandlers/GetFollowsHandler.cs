﻿using AutoMapper;
using Follow.Data.Database;
using Follow.Data.Services.Models.Follow;
using Follow.Data.Services.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Follow.Data.Services.QueryHandlers
{
    public class GetFollowsHandler : IRequestHandler<GetFollowsQuery, IEnumerable<FollowDto>>
    {
        private readonly FollowDbContext _context;
        private readonly IMapper _mapper;

        public GetFollowsHandler(FollowDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<FollowDto>> Handle(GetFollowsQuery request, CancellationToken cancellationToken)
        {
            var dbFollows = await _context.Follows.ToListAsync(cancellationToken);
            dbFollows.Reverse();
            return _mapper.Map<IEnumerable<FollowDto>>(dbFollows);
        }
    }
}
