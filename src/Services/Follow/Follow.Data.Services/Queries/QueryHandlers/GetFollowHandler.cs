﻿using AutoMapper;
using Follow.Data.Database;
using Follow.Data.Services.Models.Follow;
using Follow.Data.Services.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Follow.Data.Services.QueryHandlers
{
    public class GetFollowHandler : IRequestHandler<GetFollowQuery, FollowDto>
    {
        private readonly FollowDbContext _context;
        private readonly IMapper _mapper;

        public GetFollowHandler(FollowDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<FollowDto> Handle(GetFollowQuery request, CancellationToken cancellationToken)
        {
            var dbFollow = await _context.Follows.FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken);
            return _mapper.Map<FollowDto>(dbFollow);
        }
    }
}
