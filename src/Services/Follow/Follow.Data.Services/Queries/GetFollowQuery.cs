﻿using MediatR;
using System.ComponentModel.DataAnnotations;
using Follow.Data.Services.Models.Follow;

namespace Follow.Data.Services.Queries
{
    public record GetFollowQuery([Required] int Id) : IRequest<FollowDto>;
}
