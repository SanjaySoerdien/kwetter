﻿using MediatR;
using Follow.Data.Services.Models.Follow;

namespace Follow.Data.Services.Queries
{
    public record GetFollowsQuery() : IRequest<IEnumerable<FollowDto>>;
}
