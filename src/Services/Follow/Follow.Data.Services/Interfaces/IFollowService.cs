﻿using Follow.Data.Services.Models.Follow;

namespace Follow.Data.Services.Interfaces
{
    public interface IFollowService
    {
        Task<IEnumerable<FollowDto>> GetAsync();
        Task<FollowDto> GetByIdAsync(int id);
        Task<FollowDto?> CreateAsync(FollowCreateDto model);
        Task<bool> DeleteByIdAsync(int id);
    }
}
