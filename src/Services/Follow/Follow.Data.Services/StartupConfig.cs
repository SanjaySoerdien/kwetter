﻿using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Follow.Data.Services.Infrastructure;
using Follow.Data.Services.Interfaces;
using Follow.Data.Services.Services;

namespace Follow.Data.Services;
public static class StartupConfig
{
    public static IServiceCollection AddFollowDataServices(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddServices()
            .AddMapper(configuration)
            .AddFollowData(configuration)
            .AddScoped<ISeederService, SeederService>()
            .AddMediatR(typeof(StartupConfig).Assembly);

        return services;
    }
}
