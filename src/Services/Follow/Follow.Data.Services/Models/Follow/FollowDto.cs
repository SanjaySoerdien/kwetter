﻿using System.ComponentModel.DataAnnotations;

namespace Follow.Data.Services.Models.Follow;

public record FollowDto(

    [Required]
    int Id = default!,

    [Required]
    Guid? FollowerId = default,

    [Required]
    Guid? FollowedId = default
);
