﻿using System.ComponentModel.DataAnnotations;

namespace Follow.Data.Services.Models.Follow;

public record FollowCreateDto(

    [Required]
    Guid? FollowerId = default,

    [Required]
    Guid? FollowedId = default
);
