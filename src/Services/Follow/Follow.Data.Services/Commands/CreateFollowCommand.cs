﻿using Follow.Data.Services.Models.Follow;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Follow.Data.Services.Commands
{
    public record CreateFollowCommand([Required] FollowCreateDto Model) : IRequest<FollowDto?>;
}
