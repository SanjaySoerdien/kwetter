﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Follow.Data.Services.Commands
{
    public record DeleteFollowCommand([Required] int Id) : IRequest<bool>;
}
