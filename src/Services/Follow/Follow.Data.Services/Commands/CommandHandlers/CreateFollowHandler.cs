﻿using AutoMapper;
using Follow.Data.Database;
using Follow.Data.Services.Commands;
using Follow.Data.Services.Models.Follow;
using Kwetter.Events.FollowEvents;
using MassTransit;
using MediatR;
using Microsoft.EntityFrameworkCore;
using FollowModel = Follow.Data.Models.Follow;

namespace Follow.Data.Services.CommandHandlers
{
    public class CreateFollowHandler : IRequestHandler<CreateFollowCommand, FollowDto?>
    {
        private readonly FollowDbContext _context;
        private readonly IMapper _mapper;
        private readonly IPublishEndpoint _publisher;

        public CreateFollowHandler(FollowDbContext context, IMapper mapper, IPublishEndpoint publisher)
        {
            _context = context;
            _mapper = mapper;
            _publisher = publisher;
        }

        public async Task<FollowDto?> Handle(CreateFollowCommand request, CancellationToken cancellationToken)
        {
            var followToAdd = _mapper.Map<FollowModel>(request.Model);
            if (await _context.Follows.AnyAsync(x => x.FollowerId == followToAdd.FollowerId && x.FollowedId == followToAdd.FollowedId, cancellationToken)) return null;
            await _context.AddAsync(followToAdd, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            await _publisher.Publish(new FollowCreatedEvent(FollowerId: followToAdd.FollowerId, FollowedId: followToAdd.FollowedId), cancellationToken);

            return _mapper.Map<FollowDto>(followToAdd);
        }
    }
}
