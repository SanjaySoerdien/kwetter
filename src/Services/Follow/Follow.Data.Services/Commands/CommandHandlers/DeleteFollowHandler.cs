﻿using Follow.Data.Database;
using Follow.Data.Services.Commands;
using Kwetter.Events.FollowEvents;
using MassTransit;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Follow.Data.Services.CommandHandlers
{
    public class DeleteFollowHandler : IRequestHandler<DeleteFollowCommand, bool>
    {
        private readonly FollowDbContext _context;
        private readonly IPublishEndpoint _publisher;

        public DeleteFollowHandler(FollowDbContext context, IPublishEndpoint publisher)
        {
            _context = context;
            _publisher = publisher;
        }

        public async Task<bool> Handle(DeleteFollowCommand request, CancellationToken cancellationToken)
        {
            var dbFollow = await _context.Follows.FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken);
            if (dbFollow == null) return false;
            _context.Follows.Remove(dbFollow);
            await _context.SaveChangesAsync(cancellationToken);
            await _publisher.Publish(new FollowDeletedEvent(FollowerId: dbFollow.FollowerId, FollowedId: dbFollow.FollowedId), cancellationToken);

            return true;
        }
    }
}
