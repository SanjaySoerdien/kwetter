﻿using AutoMapper;
using Follow.Data.Services.Models.Follow;
using FollowModel = Follow.Data.Models.Follow;

namespace Follow.Data.Services.Infrastructure
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<FollowModel, FollowDto>().ReverseMap();
            CreateMap<FollowModel, FollowCreateDto>().ReverseMap();
        }
    }
}
