﻿using Profile.Command.Data.Service.Interfaces;

namespace Profile.Command.API.Infrastructure;

public class Seeder : IHostedService
{
    private readonly ISeederService _seedService;

    public Seeder(IServiceProvider serviceProvider)
    {
        _seedService = serviceProvider.CreateScope().ServiceProvider.GetRequiredService<ISeederService>();
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        await _seedService.SeedAsync();
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}
