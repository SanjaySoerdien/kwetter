using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Profile.Command.Data.Models;
using Profile.Command.Data.Service.Interfaces;

namespace Profile.Command.API.Controllers;
[Authorize]
[ApiController]
[Route("profile")]
public class ProfileCommandController : ControllerBase
{
    private readonly IProfileCommandService _profileCommandService;

    public ProfileCommandController(IProfileCommandService profileCommandService)
    {
        _profileCommandService = profileCommandService;
    }

    [AllowAnonymous]
    [HttpPost]
    public async Task<IActionResult> CreateAsync([FromBody] ProfileCreatedDto dto)
    {
        var profileDto = await _profileCommandService.CreateAsync(dto);
        if (profileDto == null) return BadRequest("Unable to create profile");
        return Created($"api/profile/{profileDto.Id}", profileDto);
    }

    [AllowAnonymous]
    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateByIdAsync([FromRoute] Guid id, [FromBody] ProfileUpdatedDto model)
    {
        var updateSucceeded = await _profileCommandService.UpdateByIdAsync(id, model);

        if (!updateSucceeded) return BadRequest("Unable to update tweet");
        return Ok();
    }

    [AllowAnonymous]
    [HttpDelete("{Id}")]
    public async Task<IActionResult> DeleteByIdAsync([FromRoute] Guid id)
    {
        var IsDeleted = await _profileCommandService.DeleteByIdAsync(id);

        if (IsDeleted)
        {
            return NoContent();
        }

        return BadRequest("Unable to delete tweet");
    }

    [AllowAnonymous]
    [HttpGet("{id}/{minutes}")]
    public async Task<IActionResult> GetOldByIdAndTimeAsync(Guid id, int minutes)
    {
        var profileDto = await _profileCommandService.GetByIdAndTimeAsync(id, minutes);

        if (profileDto is null)
        {
            return BadRequest("Unable to retrieve tweet");
        }

        return Ok(profileDto);
    }
}
