using Profile.Command.API.Infrastructure;
using Profile.Command.Data.Service;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddProfileServiceData(builder.Configuration);

builder.Services.AddControllers();
builder.Services.AddServices(builder.Configuration)
    .AddSwagger();

var app = builder.Build()
                .InitApp();


app.Run();
