﻿using MediatR;
using Profile.Command.Data.Models;

namespace Profile.Command.Data.Commands;

public record ProfileUpdatedCommand
(
    Guid Id,
    ProfileUpdatedDto Model
) : IRequest<bool>;
