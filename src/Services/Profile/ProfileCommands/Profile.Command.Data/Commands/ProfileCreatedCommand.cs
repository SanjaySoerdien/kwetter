﻿using MediatR;
using Profile.Command.Data.Models;
using System.ComponentModel.DataAnnotations;

namespace Profile.Command.Data.Commands;

public record ProfileCreatedCommand([Required] ProfileCreatedDto Model) : IRequest<ProfileDto>;
