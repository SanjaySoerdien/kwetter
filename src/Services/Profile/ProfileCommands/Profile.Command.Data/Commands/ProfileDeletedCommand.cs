﻿using MediatR;

namespace Profile.Command.Data.Commands;

public record ProfileDeletedCommand(Guid Id) : IRequest<bool>;
