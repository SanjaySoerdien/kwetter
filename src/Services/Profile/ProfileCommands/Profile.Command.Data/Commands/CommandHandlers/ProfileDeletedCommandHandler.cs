﻿using EventStore.Client;
using Kwetter.Events.ProfileEvents;
using MassTransit;
using MediatR;
using Profile.Command.Data.Helpers;

namespace Profile.Command.Data.Commands.CommandHandlers;

public class ProfileDeletedCommandHandler : IRequestHandler<ProfileDeletedCommand, bool>
{
    private readonly EventStoreClient _eventStoreClient;
    private readonly IPublishEndpoint _publisher;

    public ProfileDeletedCommandHandler(EventStoreClient eventStoreClient, IPublishEndpoint publisher)
    {
        _eventStoreClient = eventStoreClient;
        _publisher = publisher;
    }

    public async Task<bool> Handle(ProfileDeletedCommand request, CancellationToken cancellationToken)
    {
        var @event = new ProfileDeletedEvent(request.Id);
        try
        {
            var result = await _eventStoreClient.AppendToStreamAsync(StreamNameHelper.GetStreamName(request.Id),
                    StreamState.StreamExists
                    , new[] { ProfileEventStoreDbSerializer.ToJsonEventData(@event) }, cancellationToken: cancellationToken);
            await _eventStoreClient.TombstoneAsync(StreamNameHelper.GetStreamName(request.Id), result.NextExpectedStreamRevision, cancellationToken: cancellationToken);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return false;
        }

        await _publisher.Publish(@event, cancellationToken);

        return true;
    }
}
