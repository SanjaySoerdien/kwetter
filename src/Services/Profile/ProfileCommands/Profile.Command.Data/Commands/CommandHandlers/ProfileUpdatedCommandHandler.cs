﻿using EventStore.Client;
using Kwetter.Events.ProfileEvents;
using MassTransit;
using MediatR;
using Profile.Command.Data.Helpers;
using Profile.Command.Data.Models;

namespace Profile.Command.Data.Commands.CommandHandlers;

public record ProfileUpdatedCommandHandler : IRequestHandler<ProfileUpdatedCommand, bool>
{
    private readonly EventStoreClient _eventStoreClient;
    private readonly IPublishEndpoint _publisher;

    public ProfileUpdatedCommandHandler(EventStoreClient eventStoreClient, IPublishEndpoint publisher)
    {
        _eventStoreClient = eventStoreClient;
        _publisher = publisher;
    }

    public async Task<bool> Handle(ProfileUpdatedCommand request, CancellationToken cancellationToken)
    {
        var events = CreateEventsFromDto(request.Id, request.Model);
        var dbEvents = CreateEventDatasFromEvents(events);

        try
        {
            await _eventStoreClient.AppendToStreamAsync(StreamNameHelper.GetStreamName(request.Id),
                            StreamState.StreamExists,
                            dbEvents,
                            cancellationToken: cancellationToken);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return false;
        }


        foreach (var @event in events)
        {
            await _publisher.Publish(Convert.ChangeType(@event, @event.GetType()), cancellationToken);
        }

        return true;
    }

    private static IEnumerable<IProfileEvent> CreateEventsFromDto(Guid Id, ProfileUpdatedDto dto)
    {
        var profileEventNamespace = typeof(IProfileEvent).Namespace;
        var nonNullProps = dto.GetType().GetProperties()
                        .Where(prop => prop.GetValue(dto) != null);
        foreach (var prop in nonNullProps)
        {
            string typeString = $"{profileEventNamespace}.Profile{prop.Name}ChangedEvent, {typeof(IProfileEvent).Assembly.FullName}";
            Type type = Type.GetType(typeString)!;
            yield return (IProfileEvent)Activator.CreateInstance(type, Id, Convert.ChangeType(prop.GetValue(dto)!, prop.PropertyType))!;
        }
    }

    private static IEnumerable<EventData> CreateEventDatasFromEvents(IEnumerable<IProfileEvent> events)
    {
        foreach (var @event in events)
        {
            yield return ProfileEventStoreDbSerializer.ToJsonEventData(@event);
        }
    }
}
