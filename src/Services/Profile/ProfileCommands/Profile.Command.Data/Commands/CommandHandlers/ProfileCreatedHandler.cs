﻿using AutoMapper;
using EventStore.Client;
using Kwetter.Events.ProfileEvents;
using MassTransit;
using MediatR;
using Profile.Command.Data.Helpers;
using Profile.Command.Data.Models;

namespace Profile.Command.Data.Commands.CommandHandlers;

public class ProfileCreatedHandler : IRequestHandler<ProfileCreatedCommand, ProfileDto?>
{
    private readonly EventStoreClient _eventStoreClient;
    private readonly IMapper _mapper;
    private readonly IPublishEndpoint _publisher;


    public ProfileCreatedHandler(EventStoreClient eventStoreClient, IMapper mapper, IPublishEndpoint publisher)
    {
        _eventStoreClient = eventStoreClient;
        _mapper = mapper;
        _publisher = publisher;
    }

    public async Task<ProfileDto?> Handle(ProfileCreatedCommand request, CancellationToken cancellationToken)
    {
        var @event = _mapper.Map<ProfileCreatedEvent>(request.Model);
        try
        {
            await _eventStoreClient.AppendToStreamAsync(StreamNameHelper.GetStreamName(request.Model.Id),
             StreamState.NoStream,
             new[] { ProfileEventStoreDbSerializer.ToJsonEventData(@event) }, cancellationToken: cancellationToken);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return null;
        }

        await _publisher.Publish(@event, cancellationToken);

        return _mapper.Map<ProfileDto>(@event);
    }
}
