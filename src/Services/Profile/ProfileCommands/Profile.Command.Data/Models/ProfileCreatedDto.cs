﻿using System.ComponentModel.DataAnnotations;

namespace Profile.Command.Data.Models;

public record ProfileCreatedDto
{
    public Guid Id { get; set; } = Guid.NewGuid();
    [Required]
    public string Username { get; set; } = default!;
    [Required]
    public string Usertag { get; set; } = default!;
    public string? Bio { get; set; } = default;
    public string? Location { get; set; } = default;
    public string? Website { get; set; } = default;
    public DateTime DateCreated { get; set; } = DateTime.Now;
}
