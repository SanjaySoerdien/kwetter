﻿using Kwetter.Events.ProfileEvents;
using System.ComponentModel.DataAnnotations;

namespace Profile.Command.Data.Models;

public record ProfileModel
{
    [Required]
    public Guid Id { get; set; }
    [Required]
    public string Username { get; set; } = default!;
    [Required]
    public string Usertag { get; set; } = default!;
    public string? Bio { get; set; } = default;
    public string? Location { get; set; } = default;
    public string? Website { get; set; } = default;
    public DateTime DateCreated { get; set; } = DateTime.Now;

    public static ProfileModel Apply(ProfileModel? entity, IProfileEvent @event)
    {
        return @event switch
        {
            ProfileCreatedEvent e => new ProfileModel
            {
                Id = e.Id,
                Usertag = e.Usertag,
                Bio = e.Bio,
                Location = e.Location,
                Website = e.Website,
                DateCreated = e.DateCreated
            },

            ProfileLocationChangedEvent e =>
            entity! with
            {
                Location = e.Location
            },

            ProfileBioChangedEvent e =>
            entity! with
            {
                Bio = e.Bio
            },

            ProfileWebsiteChangedEvent e =>
            entity! with
            {
                Website = e.Website
            },

            ProfileUsertagChangedEvent e =>
            entity! with
            {
                Usertag = e.Usertag
            },
            ProfileUsernameChangedEvent e =>
            entity! with
            {
                Username = e.Username
            },
            _ => throw new NotImplementedException("Event not implemented")
        };
    }
}
