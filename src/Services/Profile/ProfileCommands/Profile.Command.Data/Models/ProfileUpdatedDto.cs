﻿namespace Profile.Command.Data.Models;

public record ProfileUpdatedDto
{
    public string? Usertag { get; set; } = default;
    public string? Username { get; set; } = default;
    public string? Bio { get; set; } = default;
    public string? Location { get; set; } = default;
    public string? Website { get; set; } = default;
}
