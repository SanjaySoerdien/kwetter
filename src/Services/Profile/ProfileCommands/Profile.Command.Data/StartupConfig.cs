﻿using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Profile.Command.Data.Infrastructure;

namespace Profile.Command.Data;
public static class StartupConfig
{
    public static IServiceCollection AddProfileCommandsData(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddServices()
            .AddEventStoreDb(configuration)
            .AddRabbitMQMassTransit(configuration)
            .AddMediatR(typeof(StartupConfig).Assembly);

        return services;
    }
}
