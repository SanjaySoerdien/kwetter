﻿namespace Profile.Command.Data.Helpers
{
    public static class StreamNameHelper
    {
        public static string GetStreamName(Guid streamId)
        {
            return $"Profile-{streamId}";
        }
    }
}
