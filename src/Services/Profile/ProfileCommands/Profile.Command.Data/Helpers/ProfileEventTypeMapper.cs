﻿using System.Collections.Concurrent;

namespace Profile.Command.Data.Helpers;
public static class ProfileEventTypeMapper
{
    private static readonly ConcurrentDictionary<string, Type?> typeMap = new();
    private static readonly ConcurrentDictionary<Type, string> typeNameMap = new();

    public static string ToName<TEventType>() => ToName(typeof(TEventType));

    public static string ToName(Type eventType) => typeNameMap.GetOrAdd(eventType, _ =>
    {
        var eventTypeName = eventType.FullName!;

        typeMap.AddOrUpdate(eventTypeName, eventType, (_, _) => eventType);

        return eventTypeName;
    });

    public static Type? ToType(string eventTypeName) => typeMap.GetOrAdd(eventTypeName, _ =>
    {
        var type = TypeProvider.GetFirstMatchingTypeFromCurrentDomainAssembly(eventTypeName);

        if (type == null)
            return null;

        typeNameMap.AddOrUpdate(type, eventTypeName, (_, _) => eventTypeName);

        return type;
    });
}
