﻿using EventStore.Client;
using Kwetter.Events.ProfileEvents;
using Newtonsoft.Json;
using System.Text;

namespace Profile.Command.Data.Helpers;
public static class ProfileEventStoreDbSerializer
{
    public static T? Deserialize<T>(this ResolvedEvent resolvedEvent) where T : class =>
        Deserialize(resolvedEvent) as T;

    public static object? Deserialize(this ResolvedEvent resolvedEvent)
    {
        // get type
        var eventType = ProfileEventTypeMapper.ToType(resolvedEvent.Event.EventType);

        if (eventType == null)
            return null;

        // deserialize event
        return JsonConvert.DeserializeObject(
            Encoding.UTF8.GetString(resolvedEvent.Event.Data.Span),
            eventType
        )!;
    }

    public static EventData ToJsonEventData(this IProfileEvent @event) =>
        new(
            Uuid.NewUuid(),
            ProfileEventTypeMapper.ToName(@event.GetType()),
            Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(@event))
        );
}
