﻿using Kwetter.Events.ProfileEvents;
using Profile.Command.Data.Helpers;
using System.Text;
using System.Text.Json;

namespace Profile.Command.Data.Factory;

public static class EventFactory
{
    public static IProfileEvent GetProfileEventFromEventType(string type, byte[] bytes)
    {
        IProfileEvent? @event = null;

        if (type == ProfileEventTypeMapper.ToName<ProfileCreatedEvent>())
            @event = JsonSerializer.Deserialize<ProfileCreatedEvent>(Encoding.UTF8.GetString(bytes));
        if (type == ProfileEventTypeMapper.ToName<ProfileLocationChangedEvent>())
            @event = JsonSerializer.Deserialize<ProfileLocationChangedEvent>(Encoding.UTF8.GetString(bytes));
        if (type == ProfileEventTypeMapper.ToName<ProfileUsernameChangedEvent>())
            @event = JsonSerializer.Deserialize<ProfileUsernameChangedEvent>(Encoding.UTF8.GetString(bytes));
        if (type == ProfileEventTypeMapper.ToName<ProfileUsertagChangedEvent>())
            @event = JsonSerializer.Deserialize<ProfileUsertagChangedEvent>(Encoding.UTF8.GetString(bytes));
        if (type == ProfileEventTypeMapper.ToName<ProfileWebsiteChangedEvent>())
            @event = JsonSerializer.Deserialize<ProfileWebsiteChangedEvent>(Encoding.UTF8.GetString(bytes));
        if (type == ProfileEventTypeMapper.ToName<ProfileBioChangedEvent>())
            @event = JsonSerializer.Deserialize<ProfileBioChangedEvent>(Encoding.UTF8.GetString(bytes));

        return @event!;
    }
}
