﻿using MediatR;
using Profile.Command.Data.Models;

namespace Profile.Command.Data.Queries;

public record ProfileByTimeQuery(Guid id, int minutes) : IRequest<ProfileModel?>;
