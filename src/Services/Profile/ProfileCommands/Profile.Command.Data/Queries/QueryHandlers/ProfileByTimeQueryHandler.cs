﻿using AutoMapper;
using EventStore.Client;
using Kwetter.Events.ProfileEvents;
using MassTransit;
using MediatR;
using Profile.Command.Data.Factory;
using Profile.Command.Data.Helpers;
using Profile.Command.Data.Models;

namespace Profile.Command.Data.Queries.QueryHandlers;

public class ProfileByTimeQueryHandler : IRequestHandler<ProfileByTimeQuery, ProfileModel?>
{
    private readonly EventStoreClient _eventStoreClient;
    private readonly IPublishEndpoint _publishEndpoint;
    private readonly IMapper _mapper;

    public ProfileByTimeQueryHandler(EventStoreClient eventStoreClient, IPublishEndpoint publishEndpoint, IMapper mapper)
    {
        _eventStoreClient = eventStoreClient;
        _publishEndpoint = publishEndpoint;
        _mapper = mapper;
    }

    public async Task<ProfileModel?> Handle(ProfileByTimeQuery request, CancellationToken cancellationToken)
    {
        var result = new ProfileModel();
        try
        {
            var events = await _eventStoreClient.ReadStreamAsync(Direction.Forwards,
                StreamNameHelper.GetStreamName(request.id),
                StreamPosition.Start,
                cancellationToken: cancellationToken)
                .ToListAsync(cancellationToken);

            var maxtime = DateTime.UtcNow.AddMinutes(-request.minutes);

            if (!events.Any()) return null;

            bool succesfullEventsFound = false;
            foreach (var @event in events.Select(@event => @event.Event).Where(x => x.Created < maxtime))
            {
                succesfullEventsFound = true;
                IProfileEvent profileEvent = EventFactory.GetProfileEventFromEventType(@event.EventType, @event.Data.ToArray());
                if (@event != null) result = ProfileModel.Apply(result, profileEvent);
            }
            if (!succesfullEventsFound) return null;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return null;
        }
        await PublishRevertEvent(result, cancellationToken);
        return result;
    }

    public async Task PublishRevertEvent(ProfileModel model, CancellationToken cancellation)
    {
        ProfileRevertedEvent @event = _mapper.Map<ProfileRevertedEvent>(model);
        await _publishEndpoint.Publish(@event, cancellation);
    }
}
