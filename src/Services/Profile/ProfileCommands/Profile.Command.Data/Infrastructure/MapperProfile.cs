﻿using Kwetter.Events.ProfileEvents;
using Profile.Command.Data.Models;

namespace Profile.Command.Data.Infrastructure
{
    public class MapperProfile : AutoMapper.Profile
    {
        public MapperProfile()
        {
            CreateMap<ProfileModel, ProfileCreatedDto>().ReverseMap();
            CreateMap<ProfileModel, ProfileDto>().ReverseMap();

            CreateMap<ProfileCreatedEvent, ProfileCreatedDto>().ReverseMap();
            CreateMap<ProfileCreatedEvent, ProfileModel>().ReverseMap();
            CreateMap<ProfileCreatedEvent, ProfileDto>().ReverseMap();
            CreateMap<ProfileModel, ProfileRevertedEvent>().ReverseMap();

        }
    }
}
