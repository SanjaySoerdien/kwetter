﻿using Kwetter.Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Profile.Command.Data.Markers;

namespace Profile.Command.Data.Infrastructure
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services
                .AddAutoMapper(typeof(MapperProfile));
            return services;
        }

        public static IServiceCollection AddEventStoreDb(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddEventStoreClient(configuration.GetConnectionString("profile-esdb"));
            return services;
        }

        public static IServiceCollection AddRabbitMQMassTransit(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddRabbitMQMassTransitWithConsumers<IAssemblyMarker>(configuration["RabbitMQSettings:Host"], configuration["ServiceSettings:ServiceName"]);
            return services;
        }
    }
}
