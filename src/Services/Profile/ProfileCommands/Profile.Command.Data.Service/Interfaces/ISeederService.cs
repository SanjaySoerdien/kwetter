﻿namespace Profile.Command.Data.Service.Interfaces;

public interface ISeederService
{
    public Task SeedAsync();
}
