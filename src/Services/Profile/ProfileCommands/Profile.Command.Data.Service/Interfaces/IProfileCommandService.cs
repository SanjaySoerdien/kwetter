﻿using Profile.Command.Data.Models;

namespace Profile.Command.Data.Service.Interfaces
{
    public interface IProfileCommandService
    {
        Task<ProfileDto> CreateAsync(ProfileCreatedDto model);
        Task<bool> UpdateByIdAsync(Guid id, ProfileUpdatedDto model);
        Task<bool> DeleteByIdAsync(Guid id);
        Task<ProfileModel?> GetByIdAndTimeAsync(Guid id, int minutes);
    }
}
