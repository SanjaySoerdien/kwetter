﻿using Microsoft.Extensions.DependencyInjection;
using Profile.Command.Data.Service.Commands.Services;
using Profile.Command.Data.Service.Interfaces;

namespace Profile.Command.Data.Service.Infrastructure
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<IProfileCommandService, ProfileCommandService>();

            return services;
        }
    }
}
