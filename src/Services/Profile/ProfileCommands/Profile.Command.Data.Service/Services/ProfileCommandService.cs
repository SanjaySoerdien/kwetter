﻿using MediatR;
using Profile.Command.Data.Commands;
using Profile.Command.Data.Models;
using Profile.Command.Data.Queries;
using Profile.Command.Data.Service.Interfaces;

namespace Profile.Command.Data.Service.Commands.Services
{
    public class ProfileCommandService : IProfileCommandService
    {
        private readonly IMediator _mediator;

        public ProfileCommandService(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<ProfileDto> CreateAsync(ProfileCreatedDto model)
        {
            return await _mediator.Send(new ProfileCreatedCommand(model));
        }

        public async Task<bool> DeleteByIdAsync(Guid id)
        {
            return await _mediator.Send(new ProfileDeletedCommand(id));
        }

        public async Task<ProfileModel?> GetByIdAndTimeAsync(Guid id, int minutes)
        {
            return await _mediator.Send(new ProfileByTimeQuery(id, minutes));
        }

        public async Task<bool> UpdateByIdAsync(Guid id, ProfileUpdatedDto model)
        {
            if (model.GetType().GetProperties()
                .Select(prop => prop.GetValue(model))
                .All(value => value == default))
            {
                return false;
            }
            return await _mediator.Send(new ProfileUpdatedCommand(id, model));
        }
    }
}
