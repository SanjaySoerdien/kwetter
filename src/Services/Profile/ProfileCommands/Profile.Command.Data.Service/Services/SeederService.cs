﻿using EventStore.Client;
using Kwetter.Events.ProfileEvents;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Profile.Command.Data.Helpers;
using Profile.Command.Data.Service.Interfaces;

namespace Profile.Command.Data.Service.Services;

public class SeederService : ISeederService
{
    private readonly IWebHostEnvironment _webHostEnvironment;
    private readonly EventStoreClient _eventStoreClient;

    public SeederService(IWebHostEnvironment webHostEnvironment, EventStoreClient eventStoreClient)
    {
        _webHostEnvironment = webHostEnvironment;
        _eventStoreClient = eventStoreClient;
    }

    public async Task SeedAsync()
    {
        if (!_webHostEnvironment.IsDevelopment())
        {
            return;
        }
        Thread.Sleep(TimeSpan.FromSeconds(20));
        var guid1 = Guid.Parse("9517795e-d74a-4a04-8a76-0e6310db8fd1");
        var guid2 = Guid.Parse("4f54eaf6-6c68-4225-aadc-1ecf3aaccaef");
        var guid3 = Guid.Parse("d65ea3be-b456-4dee-b360-6d0a267ef4b4");
        var result = await _eventStoreClient.ReadStreamAsync(Direction.Forwards, StreamNameHelper.GetStreamName(guid1), StreamPosition.Start).ReadState.WaitAsync(TimeSpan.FromSeconds(3));
        if (result == ReadState.StreamNotFound)
        {
            await _eventStoreClient.AppendToStreamAsync(StreamNameHelper.GetStreamName(guid1),
            StreamState.NoStream,
            new[] {
               ProfileEventStoreDbSerializer.ToJsonEventData(new ProfileCreatedEvent(
                    guid1,
                    "@JohnDoe",
                    "Janeoe"
                    )
                {
                    Bio = "Hello i am john doe. i do stuff!",
                    Website = "someurl",
                    Location = "Netherlands"
                })
            });

            await _eventStoreClient.AppendToStreamAsync(StreamNameHelper.GetStreamName(guid2),
            StreamState.NoStream,
            new[] {
                    ProfileEventStoreDbSerializer.ToJsonEventData(new ProfileCreatedEvent(
                    guid2,
                    "@JaneDoe",
                    "JaneDoe"
                    )
                {
                    Bio = "Jane doe in the building",
                    Website = "urltodooo",
                    Location = "AMERIKA"
                })
                });

            await _eventStoreClient.AppendToStreamAsync(StreamNameHelper.GetStreamName(guid3),
            StreamState.NoStream,
            new[] {
                    ProfileEventStoreDbSerializer.ToJsonEventData(new ProfileCreatedEvent(
                    guid3,
                    "@JohnSmith",
                    "JaneSmith"
                    )
                {
                    Bio = "I am JohnSmith. this is my bio.",
                    Website = "thehub.com",
                    Location = "England"
                })
            });
        }
    }
}
