﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Profile.Command.Data.Service.Infrastructure;
using Profile.Command.Data.Service.Interfaces;
using Profile.Command.Data.Service.Services;

namespace Profile.Command.Data.Service;
public static class StartupConfig
{
    public static IServiceCollection AddProfileServiceData(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddServices()
            .AddProfileCommandsData(configuration)
            .AddScoped<ISeederService, SeederService>();
        return services;
    }
}
