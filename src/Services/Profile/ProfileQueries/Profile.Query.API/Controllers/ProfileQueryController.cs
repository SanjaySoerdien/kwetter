using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Profile.Query.Data.Service.Interface;

namespace Profile.Query.API.Controllers;

[AllowAnonymous]
[ApiController]
[Route("profile")]
public class ProfileQueryController : ControllerBase
{
    private readonly IProfileQueryService _profileService;

    public ProfileQueryController(IProfileQueryService profileService)
    {
        _profileService = profileService;
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetByIdAsync([FromRoute] Guid id)
    {
        var profileDto = await _profileService.GetByIdAsync(id);

        return Ok(profileDto);
    }
}
