using Profile.Query.API.Infrastructure;
using Profile.Query.Data.Service;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddProfileQueryServiceServices(builder.Configuration);

builder.Services.AddControllers();
builder.Services.AddServices(builder.Configuration)
    .AddSwagger();

var app = builder.Build()
                .InitApp();


app.Run();