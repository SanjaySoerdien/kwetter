﻿using Profile.Query.Data.Service.Interface;

namespace Profile.Query.API.Infrastructure
{
    public class Seeder : IHostedService
    {
        private readonly ISeederService _seedService;

        public Seeder(IServiceProvider serviceProvider)
        {
            _seedService = serviceProvider.CreateScope().ServiceProvider.GetRequiredService<ISeederService>();
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await _seedService.MigrateAsync();
            await _seedService.SeedAsync();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
