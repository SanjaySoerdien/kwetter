﻿namespace Profile.Query.API.Infrastructure
{
    public static class ServiceExtensions
    {
        private readonly static string origins = "reply-origins";
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(name: origins,
                    policy =>
                    {
                        policy.WithOrigins("https://localhost:8080/", "http://localhost:8081/");
                    });
            });

            services.AddHostedService<Seeder>();

            return services;
        }

        public static IServiceCollection AddSwagger(this IServiceCollection services)
        {
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();

            return services;
        }

        public static WebApplication InitApp(this WebApplication app)
        {
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseCors(origins);
            app.MapControllers();
            return app;
        }
    }
}
