﻿namespace Profile.Query.Data.Service.Models
{
    public record ProfileDto
    {
        public string Usertag { get; set; } = default!;
        public string Bio { get; set; } = default!;
        public string Location { get; set; } = default!;
        public string Website { get; set; } = default!;
        public int Follows { get; set; } = 0;
        public int Followers { get; set; } = 0;
        public DateTime DateCreated { get; set; } = DateTime.Now;
    }
}
