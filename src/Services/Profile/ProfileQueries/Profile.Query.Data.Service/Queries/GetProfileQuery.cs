﻿using MediatR;
using Profile.Query.Data.Service.Models;
using System.ComponentModel.DataAnnotations;

namespace Profile.Query.Data.Service;

public record GetProfileQuery([Required] Guid Id) : IRequest<ProfileDto>;
