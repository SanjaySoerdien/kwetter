﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Profile.Query.Data.Database;
using Profile.Query.Data.Service.Models;

namespace Profile.Query.Data.Service.Queries.QueryHandler
{
    public class GetProfileQueryHandler : IRequestHandler<GetProfileQuery, ProfileDto>
    {
        private readonly ProfileDbContext _context;
        private readonly IMapper _mapper;

        public GetProfileQueryHandler(ProfileDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ProfileDto> Handle(GetProfileQuery request, CancellationToken cancellationToken)
        {
            var dbProfile = await _context.Profiles.FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken);
            return _mapper.Map<ProfileDto>(dbProfile);
        }
    }
}
