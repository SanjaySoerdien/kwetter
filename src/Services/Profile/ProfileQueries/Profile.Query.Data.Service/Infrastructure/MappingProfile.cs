﻿using Kwetter.Events.ProfileEvents;
using Profile.Query.Data.Service.Models;
using AutoMapperProfile = AutoMapper;
using ProfileModel = Profile.Query.Data.Models.Profile;

namespace Profile.Query.Data.Service.Infrastructure;

public class MappingProfile : AutoMapperProfile.Profile
{
    public MappingProfile()
    {
        CreateMap<ProfileModel, ProfileDto>().ReverseMap();
        CreateMap<ProfileCreatedEvent, ProfileModel>().ReverseMap();
        CreateMap<ProfileModel, ProfileRevertedEvent>().ReverseMap();
    }
}
