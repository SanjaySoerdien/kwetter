﻿using Profile.Query.Data.Service.Models;

namespace Profile.Query.Data.Service.Interface
{
    public interface IProfileQueryService
    {
        Task<ProfileDto> GetByIdAsync(Guid id);
    }
}
