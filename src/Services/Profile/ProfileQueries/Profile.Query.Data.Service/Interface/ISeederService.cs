﻿namespace Profile.Query.Data.Service.Interface
{
    public interface ISeederService
    {
        public Task MigrateAsync();
        public Task SeedAsync();
    }
}
