﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Profile.Query.Data.Database;
using Profile.Query.Data.Service.Interface;
using ProfileModel = Profile.Query.Data.Models.Profile;


namespace Profile.Query.Data.Service.Services
{
    public class SeederService : ISeederService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public SeederService(IServiceProvider serviceProvider, IWebHostEnvironment webHostEnvironment)
        {
            _serviceProvider = serviceProvider.CreateScope().ServiceProvider;
            _webHostEnvironment = webHostEnvironment;
        }

        public async Task MigrateAsync()
        {
            var context = _serviceProvider.GetRequiredService<ProfileDbContext>();
            await context.Database.MigrateAsync();
        }

        public async Task SeedAsync()
        {
            var context = _serviceProvider.GetRequiredService<ProfileDbContext>();
            if (!await context.Profiles.AnyAsync())
            {
                context.AddRange(new List<ProfileModel>()
                {
                    new ProfileModel { Id = Guid.Parse("9517795e-d74a-4a04-8a76-0e6310db8fd1"), Username = "JohnDoe", Usertag = "@JohnDoe",
                    Bio = "Hello i am john doe. i do stuff!", Location = "Netherlands", Website = "someurl" },
                    new ProfileModel { Id = Guid.Parse("4f54eaf6-6c68-4225-aadc-1ecf3aaccaef"), Username = "JaneDoe", Usertag = "@JaneDoe" ,
                    Bio = "Jane doe in the building", Location = "AMERIKA", Website = "urltodooo" },
                    new ProfileModel { Id = Guid.Parse("d65ea3be-b456-4dee-b360-6d0a267ef4b4"), Username = "JohnSmith", Usertag = "@JohnSmith",
                    Bio = "I am JohnSmith. this is my bio.", Location = "England", Website = "thehub.com" },
                });
                await context.SaveChangesAsync();
            }
        }
    }
}
