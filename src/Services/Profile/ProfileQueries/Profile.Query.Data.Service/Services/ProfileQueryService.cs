﻿using MediatR;
using Profile.Query.Data.Service.Interface;
using Profile.Query.Data.Service.Models;

namespace Profile.Query.Data.Service.Services
{
    public class ProfileQueryService : IProfileQueryService
    {
        private readonly IMediator _mediator;

        public ProfileQueryService(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<ProfileDto> GetByIdAsync(Guid id)
        {
            return await _mediator.Send(new GetProfileQuery(id));
        }
    }
}
