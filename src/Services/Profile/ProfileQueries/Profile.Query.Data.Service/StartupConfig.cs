﻿using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Profile.Query.Data.Service.Infrastructure;
using Profile.Query.Data.Service.Interface;
using Profile.Query.Data.Service.Services;

namespace Profile.Query.Data.Service;
public static class StartupConfig
{
    public static IServiceCollection AddProfileQueryServiceServices(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddServices()
            .AddProfileQueryData(configuration)
            .AddMapper(configuration)
            .AddScoped<ISeederService, SeederService>()
            .AddMediatR(typeof(StartupConfig).Assembly);

        return services;
    }
}