﻿using Kwetter.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Profile.Query.Data.Database;
using Profile.Query.Data.Markers;

namespace Profile.Query.Data.Infrastructure
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            return services;
        }

        public static IServiceCollection AddEfCore(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ProfileDbContext>(config =>
            {
                config.UseSqlServer(configuration.GetConnectionString("profile-query-db"),
                sqlServerOptionsAction: sqlOptions =>
                {
                    sqlOptions.EnableRetryOnFailure();
                });
            });
            return services;
        }

        public static IServiceCollection AddRabbitMQMassTransit(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddRabbitMQMassTransitWithConsumers<IAssemblyMarker>(configuration["RabbitMQSettings:Host"], configuration["ServiceSettings:ServiceName"]);
            return services;
        }
    }
}
