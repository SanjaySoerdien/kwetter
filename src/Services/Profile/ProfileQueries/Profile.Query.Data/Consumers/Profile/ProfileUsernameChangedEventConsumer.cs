﻿using Kwetter.Events.ProfileEvents;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Profile.Query.Data.Database;

namespace Profile.Query.Data.Consumers;

public class ProfileUsernameChangedEventConsumer : IConsumer<ProfileUsernameChangedEvent>
{
    private readonly ProfileDbContext _context;

    public ProfileUsernameChangedEventConsumer(ProfileDbContext context)
    {
        _context = context;
    }

    public async Task Consume(ConsumeContext<ProfileUsernameChangedEvent> context)
    {
        var profile = await _context.Profiles.SingleOrDefaultAsync(t => t.Id == context.Message.Id);
        if (profile == null) return;
        profile.Username = context.Message.Username;
        await _context.SaveChangesAsync();
    }
}
