﻿using AutoMapper;
using Kwetter.Events.ProfileEvents;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Profile.Query.Data.Database;
using ProfileModel = Profile.Query.Data.Models.Profile;

namespace Profile.Query.Data.Consumers.Profile;

public class ProfileRevertedEventConsumer : IConsumer<ProfileRevertedEvent>
{
    private readonly ProfileDbContext _context;
    private readonly IMapper _mapper;

    public ProfileRevertedEventConsumer(ProfileDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task Consume(ConsumeContext<ProfileRevertedEvent> context)
    {
        var profile = await _context.Profiles.SingleOrDefaultAsync(t => t.Id == context.Message.Id);
        if (profile == null) return;
        var revertedProfile = _mapper.Map<ProfileModel>(context.Message);
        profile.DateCreated = revertedProfile.DateCreated;
        profile.Username = revertedProfile!.Username;
        profile.Location = revertedProfile!.Location;
        profile.Bio = revertedProfile!.Bio;
        profile.Usertag = revertedProfile!.Usertag;
        profile.Website = revertedProfile!.Website;
        await _context.SaveChangesAsync();
    }
}
