﻿using Kwetter.Events.ProfileEvents;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Profile.Query.Data.Database;

namespace Profile.Query.Data.Consumers;

public class ProfileDeleteEventConsumer : IConsumer<ProfileDeletedEvent>
{
    private readonly ProfileDbContext _context;

    public ProfileDeleteEventConsumer(ProfileDbContext context)
    {
        _context = context;
    }

    public async Task Consume(ConsumeContext<ProfileDeletedEvent> context)
    {
        var profile = await _context.Profiles.SingleOrDefaultAsync(t => t.Id == context.Message.Id);
        if (profile == null) return;
        _context.Profiles.Remove(profile);
        await _context.SaveChangesAsync();
    }
}