﻿using AutoMapper;
using Kwetter.Events.ProfileEvents;
using MassTransit;
using Profile.Query.Data.Database;
using ProfileModel = Profile.Query.Data.Models.Profile;

namespace Profile.Query.Data.Consumers;

public class ProfileCreatedEventConsumer : IConsumer<ProfileCreatedEvent>
{
    private readonly ProfileDbContext _context;
    private readonly IMapper _mapper;

    public ProfileCreatedEventConsumer(ProfileDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task Consume(ConsumeContext<ProfileCreatedEvent> context)
    {
        var profileToAdd = _mapper.Map<ProfileModel>(context.Message);
        var profile = await _context.Profiles.AddAsync(profileToAdd);
        if (profile == null) return;
        await _context.SaveChangesAsync();
    }
}
