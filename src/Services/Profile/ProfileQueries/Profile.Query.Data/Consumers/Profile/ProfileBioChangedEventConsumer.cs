﻿using Kwetter.Events.ProfileEvents;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Profile.Query.Data.Database;

namespace Profile.Query.Data.Consumers;

public class ProfileBioChangedEventConsumer : IConsumer<ProfileBioChangedEvent>
{
    private readonly ProfileDbContext _context;

    public ProfileBioChangedEventConsumer(ProfileDbContext context)
    {
        _context = context;
    }

    public async Task Consume(ConsumeContext<ProfileBioChangedEvent> context)
    {
        var profile = await _context.Profiles.SingleOrDefaultAsync(t => t.Id == context.Message.Id);
        if (profile == null) return;
        profile.Bio = context.Message.Bio;
        await _context.SaveChangesAsync();
    }
}
