﻿using Kwetter.Events.ProfileEvents;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Profile.Query.Data.Database;

namespace Profile.Query.Data.Consumers;

public class ProfileLocationChangedEventConsumer : IConsumer<ProfileLocationChangedEvent>
{
    private readonly ProfileDbContext _context;

    public ProfileLocationChangedEventConsumer(ProfileDbContext context)
    {
        _context = context;
    }

    public async Task Consume(ConsumeContext<ProfileLocationChangedEvent> context)
    {
        var profile = await _context.Profiles.SingleOrDefaultAsync(t => t.Id == context.Message.Id);
        if (profile == null) return;
        profile.Location = context.Message.Location;
        await _context.SaveChangesAsync();
    }
}
