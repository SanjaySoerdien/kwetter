﻿using Kwetter.Events.ProfileEvents;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Profile.Query.Data.Database;

namespace Profile.Query.Data.Consumers;

public class ProfileWebsiteChangedEventConsumer : IConsumer<ProfileWebsiteChangedEvent>
{
    private readonly ProfileDbContext _context;

    public ProfileWebsiteChangedEventConsumer(ProfileDbContext context)
    {
        _context = context;
    }

    public async Task Consume(ConsumeContext<ProfileWebsiteChangedEvent> context)
    {
        var profile = await _context.Profiles.SingleOrDefaultAsync(t => t.Id == context.Message.Id);
        if (profile == null) return;
        profile.Website = context.Message.Website;
        await _context.SaveChangesAsync();
    }
}
