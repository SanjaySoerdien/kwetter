﻿using Kwetter.Events.ProfileEvents;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Profile.Query.Data.Database;

namespace Profile.Query.Data.Consumers;

public class ProfileUsertagChangedEventConsumer : IConsumer<ProfileUsertagChangedEvent>
{
    private readonly ProfileDbContext _context;

    public ProfileUsertagChangedEventConsumer(ProfileDbContext context)
    {
        _context = context;
    }

    public async Task Consume(ConsumeContext<ProfileUsertagChangedEvent> context)
    {
        var profile = await _context.Profiles.SingleOrDefaultAsync(t => t.Id == context.Message.Id);
        if (profile == null) return;
        profile.Usertag = context.Message.Usertag;
        await _context.SaveChangesAsync();
    }
}
