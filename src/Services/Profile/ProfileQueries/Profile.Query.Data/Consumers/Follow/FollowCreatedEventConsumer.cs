﻿using Kwetter.Events.FollowEvents;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Profile.Query.Data.Database;

namespace Profile.Query.Data.Consumers.Follow;

public class FollowCreatedEventConsumer : IConsumer<FollowCreatedEvent>
{
    private readonly ProfileDbContext _context;

    public FollowCreatedEventConsumer(ProfileDbContext context)
    {
        _context = context;
    }

    public async Task Consume(ConsumeContext<FollowCreatedEvent> context)
    {
        var profileFollowed = await _context.Profiles.SingleOrDefaultAsync(t => t.Id == context.Message.FollowedId);
        var profileFollower = await _context.Profiles.SingleOrDefaultAsync(t => t.Id == context.Message.FollowerId);
        if (profileFollowed == null || profileFollower == null) return;
        profileFollowed.Followers += 1;
        profileFollower.Follows += 1;
        await _context.SaveChangesAsync();
    }
}
