﻿using Kwetter.Events.FollowEvents;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Profile.Query.Data.Database;

namespace Profile.Query.Data.Consumers.Follow;

public class FollowDeletedEventConsumer : IConsumer<FollowDeletedEvent>
{
    private readonly ProfileDbContext _context;

    public FollowDeletedEventConsumer(ProfileDbContext context)
    {
        _context = context;
    }

    public async Task Consume(ConsumeContext<FollowDeletedEvent> context)
    {
        var profileFollowed = await _context.Profiles.SingleOrDefaultAsync(t => t.Id == context.Message.FollowedId);
        var profileFollower = await _context.Profiles.SingleOrDefaultAsync(t => t.Id == context.Message.FollowerId);
        if (profileFollowed != null) profileFollowed.Followers += -1;
        if (profileFollower != null) profileFollower.Follows += -1;

        await _context.SaveChangesAsync();
    }
}
