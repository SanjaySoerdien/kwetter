﻿using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Profile.Query.Data.Database;

public class ProfileDbContext : DbContext
{
    public ProfileDbContext(DbContextOptions options) : base(options)
    {
    }
    public DbSet<Models.Profile> Profiles { get; set; } = default!;
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        base.OnModelCreating(modelBuilder);
    }
}
