﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Profile.Query.Data.Configuration
{
    public class ProfileConfiguration : IEntityTypeConfiguration<Models.Profile>
    {
        public void Configure(EntityTypeBuilder<Models.Profile> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => x.Id);

            builder.Property(x => x.Usertag)
                .HasMaxLength(64)
                .HasColumnName("Usertag")
                .HasColumnType("nvarchar(240)")
                .IsRequired();

            builder.Property(x => x.Location)
                .HasMaxLength(64)
                .HasColumnName("Location")
                .HasColumnType("nvarchar(64)");

            builder.Property(x => x.Website)
                .HasMaxLength(64)
                .HasColumnName("Website")
                .HasColumnType("nvarchar(64)");

            builder.Property(x => x.Bio)
                .HasMaxLength(64)
                .HasColumnName("Bio")
                .HasColumnType("nvarchar(240)");

            builder.Property(x => x.DateCreated)
                .HasColumnName("DateCreated")
                .HasColumnType("nvarchar(64)")
                .HasDefaultValueSql("getUtcDate()")
                .IsRequired();
        }
    }
}
