﻿namespace Profile.Query.Data.Models
{
    public class Profile
    {
        public Guid Id { get; set; }
        public string? Username { get; set; } = default;
        public string? Usertag { get; set; } = default;
        public string? Bio { get; set; } = default;
        public string? Location { get; set; } = default;
        public string? Website { get; set; } = default;
        public int Follows { get; set; } = 0;
        public int Followers { get; set; } = 0;
        public DateTime DateCreated { get; set; } = DateTime.Now;
    }
}
