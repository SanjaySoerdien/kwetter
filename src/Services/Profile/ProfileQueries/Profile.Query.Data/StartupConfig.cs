﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Profile.Query.Data.Infrastructure;

namespace Profile.Query.Data;
public static class StartupConfig
{
    public static IServiceCollection AddProfileQueryData(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddServices()
            .AddEfCore(configuration)
            .AddRabbitMQMassTransit(configuration);

        return services;
    }
}
