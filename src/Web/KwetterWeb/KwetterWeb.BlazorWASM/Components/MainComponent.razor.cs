﻿using KwetterWeb.BlazorWASM.Shared;
using KwetterWeb.Infrastructure.Models;
using Microsoft.AspNetCore.Components;

namespace KwetterWeb.BlazorWASM.Components
{
    public partial class MainComponent
    {
        [CascadingParameter]
        public Notification? MainComponentNotification { get; set; }

        [Inject]
        public ILogger<MainComponent>? Logger { get; set; }

        private IEnumerable<Tweet>? Tweets { get; set; } = new List<Tweet>();

        protected async override Task OnInitializedAsync()
        {
            try
            {
                Tweets = await _tweetManager.GetTweets();
                MainComponentNotification?.ProcessSucces("Succesfully fetched tweets");
            }
            catch (Exception ex)
            {
                MainComponentNotification?.ProcessError(ex);
            }
        }
    }
}
