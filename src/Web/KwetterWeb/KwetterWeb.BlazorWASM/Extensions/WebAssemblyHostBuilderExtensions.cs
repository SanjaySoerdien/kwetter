﻿using Blazored.LocalStorage;
using Blazored.Toast;
using KwetterWeb.Infrastructure.Extensions;
using KwetterWeb.Infrastructure.Managers;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;

namespace KwetterWeb.BlazorWASM.Extensions
{
    public static class WebAssemblyHostBuilderExtensions
    {

        public static WebAssemblyHostBuilder AddRootComponents(this WebAssemblyHostBuilder builder)
        {
            builder.RootComponents.Add<App>("#app");
            builder.RootComponents.Add<HeadOutlet>("head::after");

            return builder;
        }

        public static WebAssemblyHostBuilder AddClientServices(this WebAssemblyHostBuilder builder)
        {
            builder
                .Services
                .AddSingleton<IConfiguration>(builder.Configuration)
                .AddBlazoredToast()
                .AddBlazoredLocalStorage()
                .AddManagers()
                .AddAuthorization()
                .AddHttpClients();

            return builder;
        }

        public static IServiceCollection AddManagers(this IServiceCollection services)
        {
            var managers = typeof(IManager);

            var types = managers
                .Assembly
                .GetExportedTypes()
                .Where(t => t.IsClass && !t.IsAbstract)
                .Select(t => new
                {
                    Service = t.GetInterface($"I{t.Name}"),
                    Implementation = t
                })
                .Where(t => t.Service != null);

            foreach (var type in types.Where(x => managers.IsAssignableFrom(x.Service)))
            {
                services.AddTransient(type.Service!, type.Implementation);
            }

            return services;
        }


        private static IServiceCollection AddAuthorization(this IServiceCollection services)
        {
            var configuration = services.BuildServiceProvider().GetService<IConfiguration>();

            services.AddOidcAuthentication(options =>
            {
                configuration.Bind("oidc", options.ProviderOptions);
            });

            return services;
        }
    }
}
