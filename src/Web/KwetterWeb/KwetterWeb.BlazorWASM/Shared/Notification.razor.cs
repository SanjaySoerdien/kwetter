using Blazored.Toast.Services;
using Microsoft.AspNetCore.Components;

namespace KwetterWeb.BlazorWASM.Shared
{
    public partial class Notification
    {
        [Parameter]
        public RenderFragment? ChildContent { get; set; }

        [Inject]
        public ILogger<Notification>? Logger { get; set; }

        [Inject]
        public IToastService? ToastService { get; set; }

        public void ProcessSucces(string message = "", string title = "")
        {
            ToastService?.ShowSuccess(message, title);
        }

        public void ProcessError(Exception ex)
        {
            Logger?.LogError("Error:ProcessError - Type: {Type} Message: {Message}",
                ex.GetType(), ex.Message);
            ToastService?.ShowError(ex.Message);
        }
    }
}