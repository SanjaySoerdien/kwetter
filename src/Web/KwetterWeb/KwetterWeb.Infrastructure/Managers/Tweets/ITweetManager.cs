﻿using KwetterWeb.Infrastructure.Models;

namespace KwetterWeb.Infrastructure.Managers.Tweets
{
    public interface ITweetManager : IManager
    {
        Task<Tweet?> GetTweet(int id);
        Task<IEnumerable<Tweet>?> GetTweets();
        Task AddTweet(Tweet tweet);
        Task UpdateTweet(Tweet tweet, int id);
        Task DeleteTweet(int tweetId);
    }
}
