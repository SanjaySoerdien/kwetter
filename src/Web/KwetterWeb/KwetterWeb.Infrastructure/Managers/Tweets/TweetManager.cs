﻿using KwetterWeb.Infrastructure.Extensions;
using KwetterWeb.Infrastructure.Models;
using KwetterWeb.Infrastructure.Routes;
using System.Net.Http.Json;

namespace KwetterWeb.Infrastructure.Managers.Tweets
{
    public class TweetManager : ITweetManager
    {
        private readonly HttpClient publicClient;
        private readonly HttpClient authClient;

        public TweetManager(IHttpClientFactory httpClientFactory)
        {
            publicClient = httpClientFactory.CreateClient(ServiceExtensions.PublicApi);
            authClient = httpClientFactory.CreateClient(ServiceExtensions.AuthApi);
        }

        public async Task<Tweet?> GetTweet(int id)
        {
            var response = await publicClient.GetAsync(TweetEndpoints.GetTweet(id));

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Unable to retrieve tweet");
            }

            return await response.Content.ReadFromJsonAsync<Tweet>();
        }

        public async Task<IEnumerable<Tweet>?> GetTweets()
        {
            var response = await publicClient.GetAsync(TweetEndpoints.GetTweets);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Unable to retrieve tweets");
            }

            return await response.Content.ReadFromJsonAsync<IEnumerable<Tweet>>();
        }

        public async Task AddTweet(Tweet tweet)
        {
            var response = await authClient.PostAsJsonAsync(TweetEndpoints.AddTweet, tweet);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Unable to post tweet");
            }
        }

        public async Task DeleteTweet(int tweetId)
        {
            var response = await authClient.DeleteAsync(TweetEndpoints.DeleteTweet(tweetId));
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Unable to delete tweet");
            }
        }

        public async Task UpdateTweet(Tweet tweet, int id)
        {
            var response = await authClient.PutAsJsonAsync(TweetEndpoints.UpdateTweet(id), tweet);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Unable to update tweet");
            }
        }
    }
}
