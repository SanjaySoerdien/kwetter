﻿using KwetterWeb.Infrastructure.Authentication;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace KwetterWeb.Infrastructure.Extensions
{
    public static class ServiceExtensions
    {
        const string HttpClientName = "Kwetter.API";
        public readonly static string PublicApi = "KwetterApp.PublicAPI";
        public readonly static string AuthApi = "KwetterApp.AuthAPI";

        public static IServiceCollection AddHttpClients(this IServiceCollection services)
        {

            var configuration = services.BuildServiceProvider().GetRequiredService<IConfiguration>();
            services.AddScoped<AuthenticationHeaderHandler>();

            var url = configuration.GetSection("KwetterApi")["BaseUrl"];
            services.AddHttpClient(PublicApi, client => client.BaseAddress = new Uri(url));

            services.AddHttpClient(AuthApi, client =>
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("User-Agent", HttpClientName);
            })
            .AddHttpMessageHandler<BaseAddressAuthorizationMessageHandler>();

            return services;
        }

    }
}
