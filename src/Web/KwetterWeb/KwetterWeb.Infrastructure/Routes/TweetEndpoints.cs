﻿namespace KwetterWeb.Infrastructure.Routes;

public static class TweetEndpoints
{
    private const string PREFIX = "api/tweets";
    public static string GetTweet(int productId)
    {
        return $"{PREFIX}/{productId}";
    }
    public readonly static string GetTweets = $"{PREFIX}";
    public readonly static string AddTweet = $"{PREFIX}";

    public static string DeleteTweet(int productId)
    {
        return $"{PREFIX}/{productId}";
    }
    public static string UpdateTweet(int productId)
    {
        return $"{PREFIX}/{productId}";
    }
}
