﻿namespace KwetterWeb.Infrastructure.Routes;

public static class ReplyEndpoints
{
    private const string PREFIX = "api/reply";
    public static string GetReply(int productId)
    {
        return $"{PREFIX}/{productId}";
    }
    public readonly static string GetReplies = $"{PREFIX}";
    public readonly static string AddReply = $"{PREFIX}";

    public static string DeleteReply(int productId)
    {
        return $"{PREFIX}/{productId}";
    }
    public static string UpdateReply(int productId)
    {
        return $"{PREFIX}/{productId}";
    }
}
