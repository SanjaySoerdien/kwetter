﻿namespace KwetterWeb.Infrastructure.Models
{
    public record Reply
    {
        public int Id { get; set; } = default!;
        public int TweetId { get; set; } = default!;
        public string Username { get; set; } = default!;
        public string Content { get; set; } = default!;
    }
}
