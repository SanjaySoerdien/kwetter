﻿namespace KwetterWeb.Infrastructure.Models
{
    public record Tweet
    {
        public int Id { get; init; } = default!;
        public string Username { get; init; } = default!;
        public string Content { get; init; } = default!;
    }
}
