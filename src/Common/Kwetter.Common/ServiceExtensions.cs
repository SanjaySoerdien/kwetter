﻿using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Kwetter.Common;

public static class ServiceExtensions
{
    public static IServiceCollection AddEfCore<T>(this IServiceCollection services, string connectionstring) where T : DbContext
    {
        services.AddDbContext<T>(config =>
        {
            config.UseSqlServer(connectionstring,
            sqlServerOptionsAction: sqlOptions =>
            {
                sqlOptions.EnableRetryOnFailure();
            });
        });
        return services;
    }

    public static IServiceCollection AddRabbitMQMassTransitWithConsumers<T>(this IServiceCollection services, string hostname, string servicename)
    {
        services.AddMassTransit(x =>
        {
            x.AddConsumers(typeof(T).Assembly);

            x.UsingRabbitMq((context, configurator) =>
            {
                configurator.Host(hostname);
                configurator.ConfigureEndpoints(context, new KebabCaseEndpointNameFormatter(servicename, false));
            });
        });
        return services;
    }

    public static IServiceCollection AddRabbitMQMassTransit(this IServiceCollection services, string hostname, string servicename)
    {
        services.AddMassTransit(x =>
        {
            x.UsingRabbitMq((context, configurator) =>
            {
                configurator.Host(hostname);
                configurator.ConfigureEndpoints(context, new KebabCaseEndpointNameFormatter(servicename, false));
            });
        });
        return services;
    }

}
